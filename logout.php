<?php
    require_once("web.php");
    require_once(DIR_CLASS . "common/session.php");

    // 初期処理
    $session = new session();

    // セッションを切断
    $session->close();

    // ページ表示
    Header( "Location: " . URL_TOP . "index.php" );
?>

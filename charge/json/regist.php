<?php
    require_once("web.php");
    require_once(DIR_CLASS . "common/html.php");
    require_once(DIR_CLASS . "common/confirm.php");
    require_once(DIR_CLASS . "common/convert.php");
    require_once(DIR_CLASS . "bean/charge/chargeBean.php");
    require_once(DIR_CLASS . "bean/charge/chargeCheckBean.php");
    require_once(DIR_CLASS . "dao/charge/chargeDAO.php");
    require_once(DIR_CLASS . "common/responseJson.php");

    // セッションチェック
    require_once(DIR_CLASS . "filter/ajaxFilter.php");
    require_once(DIR_CLASS . "filter/sessionFilter.php");

    // 初期処理
    $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
    $session = new session();
    $bnCharge = new chargeBean();
    $resJson = new responseJson();

    // パラメータ取得
    $bnCharge->setDataAll($_POST);

    // パラメータチェック
    $bnChargeCheck = new chargeCheckBean();
    $message = $bnChargeCheck->chkRegist($bnCharge);

    // エラーの場合
    if (confirm::checkInput($message)) {
        $resJson->setArray(array('status'=>'1', 'message'=>convert::unescapeLine($message)));
        $resJson->send();
        exit;
    }

    // 担当者情報設定
    $daoCharge = new chargeDAO();
    $intCount = $daoCharge->setOne($bnCharge);

    // エラーの場合
    if ($intCount != 1) {
        $resJson->setArray(array('status'=>'1', 'message'=>'担当者データの設定に失敗しました。'));
        $resJson->send();
        exit;
    }

    $bnCharge->setData("status", "0");
    $resJson->setArray($bnCharge->getDataAll());
    $resJson->send();
?>

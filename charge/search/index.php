<?php
    require_once("web.php");
    require_once(DIR_CLASS . "common/html.php");
    require_once(DIR_CLASS . "common/confirm.php");
    require_once(DIR_CLASS . "common/convert.php");
    require_once(DIR_CLASS . "dao/store/storeDAO.php");
    require_once(DIR_CLASS . "bean/charge/searchBean.php");
    require_once(DIR_CLASS . "bean/charge/searchCheckBean.php");
    require_once(DIR_CLASS . "dao/charge/searchDAO.php");
    require_once(DIR_CLASS . "dao/type/typeAuthDAO.php");

    // セッションチェック
    require_once(DIR_CLASS . "filter/sessionFilter.php");

    // 初期処理
    $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
    $session = new session();
    $bnSearch = new searchBean();
    $arySearch = array();
    $intCount = 0;

    // パラメータ取得
    $bnSearch->setDataAll($_GET);

    // 店舗情報取得
    $daoStore = new storeDAO();
    $aryStore = $daoStore->getAllSimple();
    $aryStore = convert::escapeXSS($aryStore);
    array_unshift($aryStore, array('id' => '', 'value' => ''));

    // 権限区分取得
    $daoTypeAuth = new typeAuthDAO();
    $aryTypeAuth = $daoTypeAuth->getAllSimple();
    $aryTypeAuth = convert::escapeXSS($aryTypeAuth);

    // パラメータチェック
    $bnSearchCheck = new searchCheckBean();
    $message = $bnSearchCheck->chkSearch($bnSearch);

    // エラーの場合
    if (confirm::checkInput($message)) {
        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "charge/search/index.php");
        exit;
    }

    // 店舗情報取得
    $daoSearch = new searchDAO();
    $arySearch = $daoSearch->getAll($bnSearch);
    $arySearch = convert::escapeXSS($arySearch);
    $intCount = $daoSearch->getAllCount($bnSearch);

    // ページ表示
    require_once(DIR_VIEW . "include/chache.php");
    require_once(DIR_VIEW . "charge/search/index.php");
?>

$(function() {
    $('.input-group.date').datepicker({
      format: "yyyy-mm-dd",
      language: "ja",
      autoclose: true,
    });

    $('#search-submit').bind("click",function(){
        $('#form-search').attr('action', 'index.php');
        $('#form-search').attr('target', '_self');
        $('#form-search').attr('method', 'get');
        $('#form-search').submit();
    });

    $('.page-submit').bind("click",function(){
        $("input:hidden[name='page']").val($(this).attr("id"));
        $('#form-search').attr('action', 'index.php');
        $('#form-search').attr('target', '_self');
        $('#form-search').attr('method', 'get');
        $('#form-search').submit();
    });

    $("button:button[name='reset']").bind("click",function(){
        $('#form-search').find("textarea,:text,select").val("").end().find(":checked").prop("checked",false).trigger('onclick');
        $("input:radio[name='flug_print']:eq()").prop('checked', true);
    });

    $(document).ready(function() {
        if ($("input:hidden[name='sub']").val() == "1") {
            $('#menu').text("詳細非表示");
            $(".sub").show();
        } else {
            $('#menu').text("詳細表示");
            $(".sub").hide();
        }
    });

    $("#menu").bind("click",function(){
        if ($("input:hidden[name='sub']").val() == "1") {
            $("input:hidden[name='sub']").val("0");
            $(this).text("詳細表示");
            $(".sub").hide();
        } else {
            $("input:hidden[name='sub']").val("1");
            $(this).text("詳細非表示");
            $(".sub").show();
        }
    });

    $('.regist-modal').click(function(){
        $("#top-message").empty();
        $("#myModalRegist #regist-message").empty();
        $("#myModalRegist #store_name").text($("#store-name").text());
        $("#myModalRegist #charge_name").text($("#charge-name").text());
        $("#myModalRegist [name='customer']").val("");
        $("#myModalRegist [name='amount']").val("");
        $("#myModalRegist [name='tax']").val("");
        $("#myModalRegist [name='proviso']").val("");
        $("#myModalRegist [name='note']").val("");

        var row = $(this).closest('tr').index();
        var col = this.cellIndex;
        $('#myModalRegist').modal('show');
    });

    $('.regist-submit').click(function(){
        $("#regist-message").empty();
        $.ajax({
            url:'/receipt/json/regist.php',
            type:'POST',
            data:$("#form-regist").serialize(),
            dataType:'json',
            timeout:1000,

        }).done(function(data) {
            if (data["status"] == 0) {
                $('#myModalRegist').modal('hide');
                location.reload();
            } else if (data["status"] == 1) {
                $("#regist-message").append('<div class=\"alert alert-danger\"></div>');
                $("#regist-message .alert").html(data["message"]);
            } else if (data["status"] == 2) {
                location.reload();
            } else {
                $("#regist-message").append('<div class=\"alert alert-danger\">領収書データの設定に失敗しました。</div>');
            }

        }).fail(function(data) {
            $("#regist-message").append('<div class=\"alert alert-danger\">領収書データの設定に失敗しました。</div>');

        }).always(function(data) {
        });
    });

    $('.edit-modal').click(function(){
        $("#top-message").empty();
        $("#myModalEdit #edit-message").empty();
        $("#myModalEdit [name='id']").val("");
        $("#myModalEdit #receipt_number").text("");
        $("#myModalEdit #date_issue").text("");
        $("#myModalEdit #store_name").text("");
        $("#myModalEdit #charge_name").text("");
        $("#myModalEdit [name='customer']").val("");
        $("#myModalEdit [name='amount']").val("");
        $("#myModalEdit [name='tax']").val("");
        $("#myModalEdit [name='proviso']").val("");
        $("#myModalEdit [name='note']").val("");
        $("#myModalEdit #flug_print").text("");

        $.ajax({
            url:'/receipt/json/detail.php',
            type:'POST',
            data:{id: $(this).data('id')},
            dataType:'json',
            timeout:1000,

        }).done(function(data) {
            if (data["status"] == 0) {
                $("#myModalEdit [name='id']").val(data["id"]);
                $("#myModalEdit #receipt_number").text(data["receipt_number"]);
                $("#myModalEdit #date_issue").text(data["date_issue"]);
                $("#myModalEdit #store_name").text(data["store_name"]);
                $("#myModalEdit #charge_name").text(data["charge_name"]);
                $("#myModalEdit [name='customer']").val(data["customer"]);
                $("#myModalEdit [name='id_type_honorific']").val(data["id_type_honorific"]);
                $("#myModalEdit [name='amount']").val(data["amount"]);
                $("#myModalEdit [name='tax']").val(data["tax"]);
                $("#myModalEdit [name='proviso']").val(data["proviso"]);
                $("#myModalEdit [name='note']").val(data["note"]);
                if (data["flug_print"] == '1') $("#myModalEdit #flug_print").text("印刷済");

                var row = $(this).closest('tr').index();
                var col = this.cellIndex;
                $('#myModalEdit').modal('show');

            } else if (data["status"] == 1) {
                $("#top-message").append('<div class=\"alert alert-danger\"></div>');
                $("#top-message .alert").html(data["message"]);
            } else if (data["status"] == 2) {
                location.reload();
            } else {
                $("#top-message").append('<div class=\"alert alert-danger\">領収書データの取得に失敗しました。</div>');
            }


        }).fail(function(data) {
            $("#top-message").append('<div class=\"alert alert-danger\">領収書データの取得に失敗しました。</div>');

        }).always(function(data) {
        });
    });

    $('.edit-submit').click(function(){
        $("#edit-message").empty();
        $.ajax({
            url:'/receipt/json/edit.php',
            type:'POST',
            data:$("#form-edit").serialize(),
            dataType:'json',
            timeout:1000,

        }).done(function(data) {
            if (data["status"] == 0) {
                $('#myModalEdit').modal('hide');
                location.reload();
            } else if (data["status"] == 1) {
                $("#edit-message").append('<div class=\"alert alert-danger\"></div>');
                $("#edit-message .alert").html(data["message"]);
            } else if (data["status"] == 2) {
                location.reload();
            } else {
                $("#edit-message").append('<div class=\"alert alert-danger\">領収書データの設定に失敗しました。</div>');
            }

        }).fail(function(data) {
            $("#edit-message").append('<div class=\"alert alert-danger\">領収書データの設定に失敗しました。</div>');

        }).always(function(data) {
        });
    });

    $('.delete-modal').click(function(){
        $("#top-message").empty();
        $("#myModalDelete [name='id']").val($(this).data('id'));

        var row = $(this).closest('tr').index();
        var col = this.cellIndex;
        $('#myModalDelete').modal('show');
    });

    $('.delete-submit').click(function(){
        $.ajax({
            url:'/receipt/json/delete.php',
            type:'POST',
            data:$("#form-delete").serialize(),
            dataType:'json',
            timeout:1000,

        }).done(function(data) {
            if (data["status"] == 0) {
                location.reload();
            } else if (data["status"] == 1) {
                $("#top-message").append('<div class=\"alert alert-danger\"></div>');
                $("#top-message .alert").html(data["message"]);
            } else if (data["status"] == 2) {
                location.reload();
            } else {
                $("#top-message").append('<div class=\"alert alert-danger\">領収書データの削除に失敗しました。</div>');
            }

        }).fail(function(data) {
            $("#top-message").append('<div class=\"alert alert-danger\">領収書データの削除に失敗しました。</div>');

        }).always(function(data) {
            $('#myModalDelete').modal('hide');
        });
    });

    $('.print-modal').click(function(){
        $("#top-message").empty();

        $.ajax({
            url:'/receipt/json/detail.php',
            type:'POST',
            data:{id: $(this).data('id')},
            dataType:'json',
            timeout:1000,

        }).done(function(data) {
            if (data["status"] == 0) {
                $("#myModalPrint [name='id']").val(data["id"]);
                $("#myModalPrint #iframe-print").attr('src', '/receipt/pdf/image.php?id=' + $("#myModalPrint [name='id']").val());

                var row = $(this).closest('tr').index();
                var col = this.cellIndex;
                $('#myModalPrint').modal('show');

            } else if (data["status"] == 1) {
                $("#top-message").append('<div class=\"alert alert-danger\"></div>');
                $("#top-message .alert").html(data["message"]);
            } else if (data["status"] == 2) {
                location.reload();
            } else {
                $("#top-message").append('<div class=\"alert alert-danger\">領収書データの取得に失敗しました。</div>');
            }


        }).fail(function(data) {
            $("#top-message").append('<div class=\"alert alert-danger\">領収書データの取得に失敗しました。</div>');

        }).always(function(data) {
        });
    });

    $('.print-confirm-submit').click(function(){
        $("#top-message").empty();
        $("#myModalPrintConfirm [name='id']").val($(this).data('id'));

        var row = $(this).closest('tr').index();
        var col = this.cellIndex;
        $('#myModalPrintConfirm').modal('show');
    });

    $('.print-submit').click(function(){
        $('#form-print').submit();
        $('#myModalPrintConfirm').modal('hide');
        $('#myModalPrint').modal('hide');
    });

    $('#pdf-list').click(function(e) {
        $('#form-search').attr('action', '/receipt/pdf/index.php');
        $('#form-search').attr('method', 'post');
        $('#form-search').attr('target', 'pdf-list');
        $('#form-search').submit();
    });

    $('#csv-list').click(function(e) {
        $('#form-search').attr('action', '/receipt/csv/list.php');
        $('#form-search').attr('method', 'post');
        $('#form-search').attr('target', 'csv-list');
        $('#form-search').submit();
    });
});

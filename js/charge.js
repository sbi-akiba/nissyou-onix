$(function(){
    $('.page-submit').bind("click",function(){
        $("input:hidden[name='page']").val($(this).attr("id"));
        $('#form-search').attr('action', 'index.php');
        $('#form-search').attr('target', '_self');
        $('#form-search').attr('method', 'get');
        $('#form-search').submit();
    });

    $('.regist-modal').click(function(){
        $("#top-message").empty();
        $("#myModalRegist #regist-message").empty();
        $("#myModalRegist [name='id_store']").val("");
        $("#myModalRegist [name='name']").val("");
        $("#myModalRegist [name='mail']").val("");
        $("#myModalRegist [name='password']").val("");
        $("#myModalRegist #type_auth_name").text("");
        $("#myModalRegist [name='flug_login_limit']").val([""]);

        var row = $(this).closest('tr').index();
        var col = this.cellIndex;
        $('#myModalRegist').modal('show');
    });

    $('.regist-submit').click(function(){
        $("#regist-message").empty();
        $.ajax({
            url:'/charge/json/regist.php',
            type:'POST',
            data:$("#form-regist").serialize(),
            dataType:'json',
            timeout:1000,

        }).done(function(data) {
            if (data["status"] == 0) {
                $('#myModalRegist').modal('hide');
                location.reload();
            } else if (data["status"] == 1) {
                $("#regist-message").append('<div class=\"alert alert-danger\"></div>');
                $("#regist-message .alert").html(data["message"]);
            } else if (data["status"] == 2) {
                location.reload();
            } else {
                $("#regist-message").append('<div class=\"alert alert-danger\">担当者データの設定に失敗しました。</div>');
            }

        }).fail(function(data) {
            $("#regist-message").append('<div class=\"alert alert-danger\">担当者データの設定に失敗しました。</div>');

        }).always(function(data) {
        });
    });

    $('.edit-modal').click(function(){
        $("#top-message").empty();
        $("#myModalEdit #edit-message").empty();
        $("#myModalEdit [name='id_store']").val("");
        $("#myModalEdit [name='name']").val("");
        $("#myModalEdit [name='mail']").val("");
        $("#myModalEdit [name='password']").val("");
        $("#myModalEdit [name='id_type_auth']").val("");
        $("#myModalEdit #type_auth_name").text("");
        $("#myModalEdit [name='flug_login_limit']").val([""]);

        $.ajax({
            url:'/charge/json/detail.php',
            type:'POST',
            data:{id: $(this).data('id')},
            dataType:'json',
            timeout:1000,

        }).done(function(data) {
            if (data["status"] == 0) {
                $("#myModalEdit [name='id']").val(data["id"]);
                $("#myModalEdit #store_name").text(data["store_name"]);
                $("#myModalEdit [name='id_store']").val(data["id_store"]);
                $("#myModalEdit [name='name']").val(data["name"]);
                $("#myModalEdit [name='mail']").val(data["mail"]);
                $("#myModalEdit [name='password']").val(data["password"]);
                $("#myModalEdit [name='id_type_auth']").val(data["id_type_auth"]);
                $("#myModalEdit #type_auth_name").text(data["type_auth_name"]);
                $("#myModalEdit [name='flug_login_limit']").val([data["flug_login_limit"]]);

                var row = $(this).closest('tr').index();
                var col = this.cellIndex;
                $('#myModalEdit').modal('show');

            } else if (data["status"] == 1) {
                $("#top-message").append('<div class=\"alert alert-danger\"></div>');
                $("#top-message .alert").html(data["message"]);
            } else if (data["status"] == 2) {
                location.reload();
            } else {
                $("#top-message").append('<div class=\"alert alert-danger\">担当者データの取得に失敗しました。</div>');
            }

        }).fail(function(data) {
            $("#top-message").append('<div class=\"alert alert-danger\">担当者データの取得に失敗しました。</div>');

        }).always(function(data) {
        });
    });

    $('.edit-submit').click(function(){
        $("#edit-message").empty();
        $.ajax({
            url:'/charge/json/edit.php',
            type:'POST',
            data:$("#form-edit").serialize(),
            dataType:'json',
            timeout:1000,

        }).done(function(data) {
            if (data["status"] == 0) {
                $('#myModalEdit').modal('hide');
                location.reload();
            } else if (data["status"] == 1) {
                $("#edit-message").append('<div class=\"alert alert-danger\"></div>');
                $("#edit-message .alert").html(data["message"]);
            } else if (data["status"] == 2) {
                location.reload();
            } else {
                $("#edit-message").append('<div class=\"alert alert-danger\">担当者データの設定に失敗しました。</div>');
            }

        }).fail(function(data) {
            $("#edit-message").append('<div class=\"alert alert-danger\">担当者データの設定に失敗しました。</div>');

        }).always(function(data) {
        });
    });
});

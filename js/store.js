$(function(){
    $('.page-submit').bind("click",function(){
        $("input:hidden[name='page']").val($(this).attr("id"));
        $('#form-search').attr('action', 'index.php');
        $('#form-search').attr('target', '_self');
        $('#form-search').attr('method', 'get');
        $('#form-search').submit();
    });

    $('.regist-modal').click(function(){
        $("#top-message").empty();
        $("#myModalRegist #regist-message").empty();
        $("#myModalRegist [name='store_number']").val("");
        $("#myModalRegist [name='name']").val("");
        $("#myModalRegist [name='zip_code']").val("");
        $("#myModalRegist [name='address_1']").val("");
        $("#myModalRegist [name='address_2']").val("");
        $("#myModalRegist [name='tel']").val("");
        $("#myModalRegist [name='fax']").val("");

        var row = $(this).closest('tr').index();
        var col = this.cellIndex;
        $('#myModalRegist').modal('show');
    });

    $('.regist-submit').click(function(){
        $("#regist-message").empty();
        $.ajax({
            url:'/store/json/regist.php',
            type:'POST',
            data:$("#form-regist").serialize(),
            dataType:'json',
            timeout:1000,

        }).done(function(data) {
            if (data["status"] == 0) {
                $('#myModalRegist').modal('hide');
                location.reload();
            } else if (data["status"] == 1) {
                $("#regist-message").append('<div class=\"alert alert-danger\"></div>');
                $("#regist-message .alert").html(data["message"]);
            } else if (data["status"] == 2) {
                location.reload();
            } else {
                $("#regist-message").append('<div class=\"alert alert-danger\">店舗データの設定に失敗しました。</div>');
            }

        }).fail(function(data) {
            $("#regist-message").append('<div class=\"alert alert-danger\">店舗データの設定に失敗しました。</div>');

        }).always(function(data) {
        });
    });

    $('.edit-modal').click(function(){
        $("#top-message").empty();
        $("#myModalEdit #edit-message").empty();
        $("#myModalEdit #store_number").text("");
        $("#myModalEdit #name").val("");
        $("#myModalEdit [name='zip_code']").val("");
        $("#myModalEdit [name='address_1']").val("");
        $("#myModalEdit [name='address_2']").val("");
        $("#myModalEdit [name='tel']").val("");
        $("#myModalEdit [name='fax']").val("");

        $.ajax({
            url:'/store/json/detail.php',
            type:'POST',
            data:{id: $(this).data('id')},
            dataType:'json',
            timeout:1000,

        }).done(function(data) {
            if (data["status"] == 0) {
                $("#myModalEdit [name='id']").val(data["id"]);
                $("#myModalEdit #store_number").text(data["store_number"]);
                $("#myModalEdit #name").val(data["name"]);
                $("#myModalEdit [name='zip_code']").val(data["zip_code"]);
                $("#myModalEdit [name='address_1']").val(data["address_1"]);
                $("#myModalEdit [name='address_2']").val(data["address_2"]);
                $("#myModalEdit [name='tel']").val(data["tel"]);
                $("#myModalEdit [name='fax']").val(data["fax"]);

                var row = $(this).closest('tr').index();
                var col = this.cellIndex;
                $('#myModalEdit').modal('show');

            } else if (data["status"] == 1) {
                $("#top-message").append('<div class=\"alert alert-danger\"></div>');
                $("#top-message .alert").html(data["message"]);
            } else if (data["status"] == 2) {
                location.reload();
            } else {
                $("#top-message").append('<div class=\"alert alert-danger\">店舗データの取得に失敗しました。</div>');
            }

        }).fail(function(data) {
            $("#top-message").append('<div class=\"alert alert-danger\">店舗データの取得に失敗しました。</div>');

        }).always(function(data) {
        });
    });

    $('.edit-submit').click(function(){
        $("#edit-message").empty();
        $.ajax({
            url:'/store/json/edit.php',
            type:'POST',
            data:$("#form-edit").serialize(),
            dataType:'json',
            timeout:1000,

        }).done(function(data) {
            if (data["status"] == 0) {
                $('#myModalEdit').modal('hide');
                location.reload();
            } else if (data["status"] == 1) {
                $("#edit-message").append('<div class=\"alert alert-danger\"></div>');
                $("#edit-message .alert").html(data["message"]);
            } else if (data["status"] == 2) {
                location.reload();
            } else {
                $("#edit-message").append('<div class=\"alert alert-danger\">店舗データの設定に失敗しました。</div>');
            }

        }).fail(function(data) {
            $("#edit-message").append('<div class=\"alert alert-danger\">店舗データの設定に失敗しました。</div>');

        }).always(function(data) {
        });
    });
});

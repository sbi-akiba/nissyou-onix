<?php
    require_once("web.php");
    require_once(DIR_CLASS . "common/confirm.php");
    require_once(DIR_CLASS . "common/convert.php");
    require_once(DIR_CLASS . "common/session.php");
    require_once(DIR_CLASS . "bean/charge/chargeBean.php");
    require_once(DIR_CLASS . "bean/charge/chargeCheckBean.php");
    require_once(DIR_CLASS . "dao/charge/chargeDAO.php");

    // 初期処理
    $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");

    // パラメータ取得
    $bnCharge = new chargeBean();
    $bnCharge->setDataAll($_POST);

    // パラメータチェック
    $bnChargeCheck = new chargeCheckBean();
    $message = $bnChargeCheck->chkLogin($bnCharge);

    // エラーの場合
    if (confirm::checkInput($message)) {
        require_once(DIR_VIEW . "index.php");
        exit;
    }

    // ユーザ情報取得
    $bnChargeTemp = new chargeBean();
    $daoCharge = new chargeDAO();
    $bnChargeTemp = $daoCharge->getLogin($bnCharge);

    // 認証した場合
    if (confirm::checkInput($bnChargeTemp->getData('id'))) {
        // ログインがロックされている場合
        if (confirm::checkComp(LOGIN_LOCK, $bnChargeTemp->getData('flug_login_limit'))) {
            $message = "ログインがロックされています。\r\n管理者へお問い合せ下さい。";
            require_once(DIR_VIEW . "index.php");
            exit;

        // ログインに失敗した場合
        } else if ($bnChargeTemp->getData('password') != convert::encrypt($bnCharge->getData('password'))) {
            // 不適合回数を設定
            $daoCharge->addMismatch($bnChargeTemp);
            $message = "ユーザIDかパスワードが一致しません。";
            require_once(DIR_VIEW . "index.php");
            exit;

        } else {
            // 不適合回数をリセット
            $daoCharge->delMismatch($bnChargeTemp);
            $bnCharge = $bnChargeTemp;
        }

    // 認証に失敗した場合
    } else {
        $message = "ユーザIDかパスワードが一致しません。";
        require_once(DIR_VIEW . "index.php");
        exit;
    }

    // セッションに格納
    $session = new session();
    $session->setDataAll($bnCharge->getDataAll());
    $session->setData("host", $_SERVER["REMOTE_ADDR"]);
    $session->setData("time", strtotime(date('Y-m-d H:i:s')));

    // ページ表示
    Header( "Location: " . URL_TOP . "receipt/search/index.php" );
?>

<?php
// バージョン設定
define("VERSION", "20180425001");

// トップURL設定
define("URL_TOP", "//onixsystem.com/");

// データURL
define("URL_DATA", "//onixsystem.com/app/data/");

// 権限
define("LEVEL_ALL_STORE", "1");
define("LEVEL_NORMAL", "2");

// ログイン制限
define("LOGIN_UNLOCK", "0");
define("LOGIN_LOCK", "1");

// PDF種類
define("PDF_LIST", "1");
define("PDF_RECEIPT", "2");
?>

<?php
require_once("define.php");

ini_set('error_reporting', E_ALL & ~E_STRICT);
ini_set('display_startup_errors', 'On');
ini_set('display_errors', 'On');
ini_set('log_errors', 'On');
ini_set('error_log', dirname(__FILE__) .  '/data/error.log');

// ルート設定
define("DIR_ROOT", dirname(__FILE__) . '/../');

// 設定ファイル
define("DIR_CONFIG", DIR_ROOT . "app/config/");

// ビューファイル
define("DIR_VIEW", DIR_ROOT . "app/view/");

// クラスファイル
define("DIR_CLASS", DIR_ROOT . "app/model/");

// ライブラリファイル
define("DIR_LIB", DIR_ROOT . "app/lib/");

// データファイル
define("DIR_DATA", DIR_ROOT . "app/data/");

// テンプレートファイル
define("DIR_TEMPLATE", DIR_ROOT . "app/template/");
?>

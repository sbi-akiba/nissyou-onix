<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>領収書システム</title>
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/bootstrap-datepicker3.min.css" rel="stylesheet">
<link href="/css/receipt.css" rel="stylesheet">

<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-datepicker.min.js"></script>
<script src="/js/bootstrap-datepicker.ja.min.js" charset="UTF-8"></script>
<script src="/js/receipt.js?<?=VERSION?>" charset="UTF-8"></script>
</head>
<body>

<?require_once(DIR_VIEW . "include/header.php")?>

<div class="container-fluid">
  <div class="page-header" style="margin-top:-20px;padding-bottom:0px;">
    <h1>領収書管理</h1>
  </div>

    <!-- 検索フォーム -->
    <div class="table-responsive">
        <p>検索条件を指定して検索ボタンをクリックして下さい。</p>
        <form action="index.php" method="get" accept-charset="utf-8" class="form-horizontal" id="form-search">
        <input type="hidden" name="sub" value="<?=$bnSearch->getData('sub')?>">
        <input type="hidden" name="output" value="<?=PDF_LIST?>">
        <input type="hidden" name="page" value="1">
        <table class="table ">
            <tr>
                <th class="col-sm-1"><p class="form-control-static">店舗名</p></th>
                <td class="col-sm-5">
                  <? if (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) { ?>
                      <?=html::pulldown("id_store", $aryStore, $bnSearch->getData('id_store'), "class=\"form-control input-md\"")?>
                  <? } else { ?>
                      <p class="form-control-static"><?=$session->getData('store_name')?></p>
                      <input type="hidden" name="id_store" value="<?=$session->getData('id_store')?>">
                  <? } ?>
                </td>
                <th class="col-sm-1"><p class="form-control-static">担当者名</p></th>
                <td class="col-sm-5">
                  <?=html::pulldown("id_charge", $aryCharge, $bnSearch->getData('id_charge'), "class=\"form-control input-md\"")?>
                </td>
            </tr>
            <tr>
                <th class="col-sm-1"><p class="form-control-static">印刷日</p></th>
                <td class="col-sm-5 form-inline">
                    <div class="input-group date"><input type="text" class="form-control" value="<?=$bnSearch->getData('date_issue_01')?>" name="date_issue_01"><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span></div>&emsp;～&emsp;
                    <div class="input-group date"><input type="text" class="form-control" value="<?=$bnSearch->getData('date_issue_02')?>" name="date_issue_02"><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span></div>
                </td>
                <th class="col-sm-1"><p class="form-control-static">印刷</p></th>
                <td class="col-sm-5">
                    <label class="radio-inline"><input type="radio" name="flug_print" value="" <?=html::match($bnSearch->getData('flug_print'), "", "checked")?>>すべて</label>
                    <label class="radio-inline"><input type="radio" name="flug_print" value="1" <?=html::match($bnSearch->getData('flug_print'), "1", "checked")?>>済</label>
                    <label class="radio-inline"><input type="radio" name="flug_print" value="0" <?=html::match($bnSearch->getData('flug_print'), "0", "checked")?>>未</label>
                </td>
            </tr>
            <tr class="sub" <?=html::match($bnSearch->getData('sub'), "0", "style=\"display: none;\"")?>>
                <th class="col-sm-1"><p class="form-control-static">お客さま名</p></th>
                <td class="col-sm-5"><input name="customer" placeholder="" class="form-control input-md" type="text" value="<?=$bnSearch->getData('customer')?>"></td>
                <th class="col-sm-1"><p class="form-control-static">品書き</p></th>
                <td class="col-sm-5"><input name="proviso" placeholder="" class="form-control input-md" type="text" value="<?=$bnSearch->getData('proviso')?>"></td>
            </tr>
            <tr class="sub" <?=html::match($bnSearch->getData('sub'), "0", "style=\"display: none;\"")?>>
                <th class="col-sm-1"><p class="form-control-static">税込金額</p></th>
                <td class="col-sm-5"><input name="amount" placeholder="" class="form-control input-md" type="text" value="<?=$bnSearch->getData('amount')?>"></td>
                <th class="col-sm-1"><p class="form-control-static">備考</p></th>
                <td class="col-sm-5"><input name="note" placeholder="" class="form-control input-md" type="text" value="<?=$bnSearch->getData('note')?>"></td>
            </tr>
            <tr>
                <td class="col-sm-12" colspan="4"></td>
            </tr>
        </table>
        <div class="text-center submit" style="margin-top:-10px;padding-bottom:30px;">
            <button type="submit" class="btn btn-primary" id="search-submit">検索</button>&emsp;<button type="button" class="btn btn-secondary" name="reset">クリア</button>&emsp;<button type="button" class="btn btn-secondary" id="menu"> <?=html::match3($bnSearch->getData('sub'), "0", "詳細表示", "詳細非表示")?></button>
        </div>
        </form>
    </div>

<div id="top-message"><? if (confirm::checkInput($message)) { ?><div class="alert alert-danger"><?=convert::unescapeLine($message)?></div><? } ?></div>

<div class="form-inline row" style="margin-top:-10px;padding-bottom:30px;">
<div class="form-group col-sm-8">
<button type="button" class="btn btn-primary regist-modal">新規登録</button>
&emsp;<button type="button" class="btn btn-primary" id="pdf-list">一覧印刷</button>
<? if (confirm::checkComp($session->getData("id_type_auth"), LEVEL_ALL_STORE)) { ?>&emsp;<button type="button" class="btn btn-primary" id="csv-list">CSV出力</button><? } ?>
</div>
<div class="form-group col-sm-4 text-right" style="font-size: 18px;margin-top:10px;">
<span class="label label-info">合計金額</span> <strong><?=convert::priceFormat($intAmount)?></strong> 円&emsp;
<span class="label label-info">合計件数</span> <strong><?=convert::priceFormat($intCount)?></strong> 件&emsp;
</div>
</div>

<div class="table-responsive">
<table class="table table-striped" id="ellipsis">
    <thead>
        <tr>
            <th width="10%">番号</th>
            <th width="10%">印刷日</th>
            <th width="12%">店舗名</th>
            <th width="9%">担当者名</th>
            <th width="9%">お客さま名</th>
            <th width="8%">税込金額</th>
            <th width="12%">品書き</th>
            <th width="12%">備考</th>
            <th width="6%">印刷</th>
            <th width="12%">操作</th>
        </tr>
    </thead>
    <tbody>
<? if (count($arySearch) <= 0) { ?>
        <tr>
          <td colspan="10" align="center">領収書情報はありません。</td>
        </tr>
<? } else {
    while(list($index, $data) = each($arySearch)) {
?>
        <tr>
            <td><?=$data['receipt_number']?></td>
            <td><?=$data['date_issue']?></td>
            <td><?=$data['store_name']?></td>
            <td><?=$data['charge_name']?></td>
            <td><?=$data['customer']?></td>
            <td>&yen<?=convert::priceFormat($data['amount'])?></td>
            <td><?=$data['proviso']?></td>
            <td><?=$data['note']?></td>
            <td><? if (confirm::checkComp($data['flug_print'], "1")) { ?><span class="label label-info" style="font-size: 12px;">印刷済</span><? } ?></td>
            <td>
<? if (confirm::checkComp($session->getData("id_type_auth"), LEVEL_ALL_STORE)) { ?>
                <button type="button" class="btn btn-primary btn-xs edit-modal" data-id="<?=$data['id']?>">編集</button>
<? } else { ?>
                <button type="button" class="btn btn-primary btn-xs edit-modal" data-id="<?=$data['id']?>" <?=html::match($data['flug_print'], "1", " disabled=\"disabled\"")?>>編集</button>
<? } ?>

                <button type="button" class="btn btn-primary btn-xs delete-modal" data-id="<?=$data['id']?>" <?=html::match($data['flug_print'], "1", " disabled=\"disabled\"")?>>削除</button>
                <button type="button" class="btn btn-primary btn-xs print-modal" data-id="<?=$data['id']?>" <?=html::match($data['flug_print'], "1", " disabled=\"disabled\"")?>>印刷</button>


            </td>
        </tr>
    <? } ?>
<? } ?>

    </tbody>
</table>
</div>

<?
    $count = count($arySearch);

    if ($count > 0) {
        $nowPage = $bnSearch->getData('page');
        $totalPage = ceil($intCount / $ini_array["receipt_search_max"]);
        $startPage = html::pageStart($nowPage, $totalPage, $ini_array["receipt_search_count"]);
        $endPage = html::pageEnd($nowPage, $totalPage, $ini_array["receipt_search_count"]);
        $startCount = (($nowPage - 1) * $ini_array["receipt_search_max"]) + 1;
        $endCount = $startCount + $count - 1;
        $nextCount = (($nowPage + 1) == $totalPage) ? $intCount - ($nowPage * $ini_array["receipt_search_max"]) : $ini_array["receipt_search_max"];
        $backCount = $ini_array["receipt_search_max"];
    }
?>

<nav>
    <ul class="pagination">

<? if ($count > 0) { ?>
    <? if ($nowPage > 1) { ?>
        <li><a href="#" class="page-submit" id="<?=$nowPage - 1?>" aria-label="前のページへ"><span aria-hidden="true">«</span></a></li>
    <? } ?>

    <? if ($endPage != 1) { ?>
        <? for ($cntPage = $startPage; $cntPage <= $endPage; $cntPage++) { ?>
            <? if ($cntPage == $nowPage) { ?>
        <li class="active"><a href="#" id="<?=$nowPage - 1?>"><?=$nowPage?></a></li>
            <?} else {?>
        <li><a href="#" class="page-submit" id="<?=$cntPage?>"><?=$cntPage?></a></li>
            <? } ?>
        <? } ?>
    <? } ?>

    <? if($nowPage < $endPage) { ?>
        <li><a href="#" class="page-submit" id="<?=$nowPage + 1?>" aria-label="次のページへ"><span aria-hidden="true">»</span></a></li>
    <? } ?>
<? } ?>
    </ul>
</nav>
</div>

<div class="modal" id="myModalRegist">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel1">新規登録</h4>
            </div>
            <div class="modal-body">
            <div id="regist-message"></div>
            <form class="form-horizontal" id="form-regist">
              <input type="hidden" name="id_store" value="<?=$session->getData('id_store')?>">
              <input type="hidden" name="id_charge" value="<?=$session->getData('id')?>">
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">店舗名</label>
                <div class="col-sm-3">
                  <p class="form-control-static" id="store_name"></p>
                </div>
                <label for="" class="col-sm-2 control-label">担当者名</label>
                <div class="col-sm-3">
                  <p class="form-control-static" id="charge_name"></p>
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">お客さま名</label>
                <div class="col-sm-4">
                  <input type="text" name="customer" id="customer" class="form-control" style="ime-mode:active;">
                </div>
                <div class="col-sm-4">
                  <?=html::pulldown("id_type_honorific", $aryTypeHonorific, "", "class=\"form-control\" id=\"id_type_honorific\"")?>
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">税込金額</label>
                <div class="col-sm-8">
                  <input type="text" name="amount" name="id" class="form-control" style="ime-mode:inactive;">
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">うち消費税額</label>
                <div class="col-sm-8">
                  <input type="text" name="tax" id="tax" class="form-control" style="ime-mode:inactive;">
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">品書き</label>
                <div class="col-sm-8">
                  <input type="text" name="proviso" id="proviso" class="form-control" style="ime-mode:active;">
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">備考</label>
                <div class="col-sm-8">
                  <textarea name="note" id="note" rows="3" class="form-control" style="ime-mode:active;"></textarea>
                  <p>備考は、領収書控にのみ印刷されます</p>
                </div>
              </div>
            </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary regist-submit">保存</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModalEdit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel1">編集</h4>
            </div>
            <div class="modal-body">
            <div id="edit-message"></div>
            <form class="form-horizontal" id="form-edit">
              <input type="hidden" name="id">
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">番号</label>
                <div class="col-sm-3">
                  <p class="form-control-static" id="receipt_number"></p>
                </div>

                <label for="" class="col-sm-2 control-label">印刷日</label>
                <div class="col-sm-3">
                  <p class="form-control-static" id="date_issue"></p>
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">店舗名</label>
                <div class="col-sm-3">
                  <p class="form-control-static" id="store_name"></p>
                </div>
                <label for="" class="col-sm-2 control-label">担当者名</label>
                <div class="col-sm-3">
                  <p class="form-control-static" id="charge_name"></p>
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">お客さま名</label>
                <div class="col-sm-4">
                  <input type="text" name="customer" id="customer" class="form-control" style="ime-mode:active;">
                </div>
                <div class="col-sm-4">
                  <?=html::pulldown("id_type_honorific", $aryTypeHonorific, "", "class=\"form-control\" id=\"id_type_honorific\"")?>
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">税込金額</label>
                <div class="col-sm-8">
                  <input type="text" name="amount" name="id" class="form-control" style="ime-mode:inactive;">
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">うち消費税額</label>
                <div class="col-sm-8">
                  <input type="text" name="tax" id="tax" class="form-control" style="ime-mode:inactive;">
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">品書き</label>
                <div class="col-sm-8">
                  <input type="text" name="proviso" id="proviso" class="form-control" style="ime-mode:active;">
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">備考</label>
                <div class="col-sm-8">
                  <textarea name="note" id="note" rows="3" class="form-control" style="ime-mode:active;"></textarea>
                  <p>備考は、領収書控にのみ印刷されます</p>
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label"></label>
                <div class="col-sm-8">
                  <p class="form-control-static"><span class="label label-info" style="font-size: 12px;" id="flug_print"></span></p>
                </div>
              </div>

            </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary edit-submit">保存</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModalDelete">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel1">削除確認</h4>
            </div>
            <div class="modal-body">
            <form class="form-horizontal" id="form-delete">
              <input type="hidden" name="id">
              <div class="form-group">
                  <div class="col-sm-11">削除しますがよろしいですか？</div>
              </div>
            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary delete-submit">削除</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">キャンセル</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModalPrint">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel1">印刷プレビュー</h4>
            </div>
            <div class="modal-body">

            <form action="/receipt/pdf/index.php" method="post" target="pdf-print" accept-charset="utf-8" class="form-horizontal" id="form-print">
              <input type="hidden" name="id">
              <input type="hidden" name="output" value="<?=PDF_RECEIPT?>">
              <div class="form-group">
                  <div class="col-sm-10 col-sm-offset-1" style="height: 400px;"><iframe src="" name="iframe-print" id="iframe-print" frameborder="0" height="100%" width="100%" scrolling="auto" style="border: 1px #dddddd solid;"></iframe></div>
              </div>

            </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary print-confirm-submit">印刷</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModalPrintConfirm">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel1">印刷確認</h4>
            </div>
            <div class="modal-body">
            <form class="form-horizontal" id="form-print-confirm">
              <input type="hidden" name="id">
              <div class="form-group">
                  <div class="col-sm-11">印刷を実行すると本データは確定され以後、取消・再編集できません。印刷を行いますか？</div>
              </div>
            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary print-submit">印刷</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">キャンセル</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>

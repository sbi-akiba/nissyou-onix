<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<title>領収書一覧</title>
<style type="text/css">
<!--
body {
  font-size: 8px;
  font-family:Meiryo, 'Lucida Grande', sans-serif;
  word-break: break-all;
  word-wrap:break-word;
}
table{
  border-collapse:collapse;
  table-layout:fixed;
}
table tr:nth-child(odd) td {
  background: #F9F9F9;
}
th {
  border-bottom: 2px solid #DDD;
}
td {
  border-bottom: 1px solid #DDD;
}
-->
</style>
</head>
<body>
<? if ($countPage == 1) { ?>
<p>
<?
echo $bnSearch->getData("id_charge");
?>
<strong>店舗名</strong>【<? if (confirm::checkInput($bnSearch->getData('id_store'))) { ?><?=html::text($aryStore, $bnSearch->getData('id_store'))?><? } ?>】　
<strong>印刷日</strong>【<? if ((confirm::checkInput($bnSearch->getData("date_issue_01"))) || (confirm::checkInput($bnSearch->getData("date_issue_02")))) { ?><?=convert::dateFormat($bnSearch->getData("date_issue_01"))?> ～ <?=convert::dateFormat($bnSearch->getData("date_issue_02"))?><? } ?>】　
<strong>担当者名</strong>【<? if (confirm::checkInput($bnSearch->getData('id_charge'))) { ?><?=html::text($aryCharge, $bnSearch->getData('id_charge'))?><? } ?>】
<br>
<strong>合計金額</strong> <?=convert::priceFormat($intAmount)?>円　
<strong>合計件数</strong> <?=convert::priceFormat($intCount)?>件
</p>
<? } ?>

<table width="1100" border="0" cellspacing="0" cellpadding="3">
    <thead>
        <tr>
            <th width="8%" height="16">番号</th>
            <th width="8%">日付</th>
            <th width="12%">店舗名</th>
            <th width="10%">担当者名</th>
            <th width="10%">お客さま名</th>
            <th width="7%">税込金額</th>
            <th width="16%">品書き</th>
            <th width="26%">備考</th>
            <th width="3%">印刷</th>
        </tr>
    </thead>
    <tbody>
<? while(list($index, $data) = each($aryData)) { ?>
        <tr>
            <td height="16"><?=$data['receipt_number']?></td>
            <td><?=$data['date_issue']?></td>
            <td><?=convert::commentCut($data['store_name'], 0, 14, "...")?></td>
            <td><?=convert::commentCut($data['charge_name'], 0, 10, "...")?></td>
            <td><?=convert::commentCut($data['customer'], 0, 10, "...")?></td>
            <td align="right"><?=convert::priceFormat($data['amount'])?></td>
            <td><?=convert::commentCut($data['proviso'], 0, 18, "...")?></td>
            <td><?=convert::commentCut($data['note'], 0, 32, "...")?></td>
            <td align="center"><? if (confirm::checkComp($data['flug_print'], "1")) { ?>済<? } ?></td>
        </tr>
<? } ?>
    </tbody>
</table>
</body>
</html>

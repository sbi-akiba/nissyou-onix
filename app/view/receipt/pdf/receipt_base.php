<table width="<?=$width?>" border="0" cellspacing="0" cellpadding="3">
  <tbody>
    <tr>
      <td height="60"></td>
    </tr>
    <tr>
      <td align="right" style="font-size: 180%">NO.<?=$bnReceipt->getData('receipt_number')?></td>
    </tr>
    <tr>
      <td style="font-size: 220%"><?=$bnReceipt->getData('customer')?> <?=$bnReceipt->getData('honorific_value')?></td>
    </tr>
    <tr>
      <td align="right" style="font-size: 180%"><?=date('Y年m月d日')?></td>
    </tr>
    <tr>
      <td height="80"></td>
    </tr>
    <tr>
      <td align="center" style="font-size: 360%">領収書<?if (confirm::checkComp($stub, 1)) { echo "（控）"; }?></td>
    </tr>
    <tr>
      <td height="120"></td>
    </tr>
    <tr>
      <td align="center">
      <table border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td align="center"></td>
            <td align="center" width="80"></td>
            <td align="center" width="20"></td>
            <td align="center" width="430"><span style="font-size: 420%"><u>金 <?=convert::priceFormat($bnReceipt->getData('amount'))?>※</u></span><br>
            <span style="font-size: 180%">但し <?=convert::mb_wordwrap($bnReceipt->getData('proviso'), 20, "<br />")?> として</span></td>
            <td align="center" width="20"></td>
            <td align="right" width="80" valign="top">
            <table border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td width="80" height="90" <? if (confirm::checkComp($stub, 0)) { ?>style="border-style: dashed dashed; border-width: 1px;"<? } ?>></td>
                </tr>
              </tbody>
            </table>
            </td>
            <td align="center"></td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
    <tr>
      <td height="80"></td>
    </tr>
    <tr>
      <td>
      <table border="0" cellspacing="0" cellpadding="3">
        <tbody>
          <tr>
            <td style="font-size: 160%">税抜金額：</td>
            <td align="right" style="font-size: 160%"><?=convert::priceFormat($bnReceipt->getData('amount') - $bnReceipt->getData('tax'))?>円</td>
          </tr>
          <tr>
            <td style="font-size: 160%">消費税額：</td>
            <td align="right" style="font-size: 160%"><?=convert::priceFormat($bnReceipt->getData('tax'))?>円</td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
    <tr>
      <td height="80"></td>
    </tr>
    <tr>
      <td align="center">
      <table border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td width="300" valign="top">
            <? if (confirm::checkComp($stub, 1)) { ?>
            <table border="0" cellspacing="0" cellpadding="3">
              <tbody>
                <tr>
                  <td style="font-size: 140%">備考：</td>
                </tr>
                <tr>
                  <td style="font-size: 140%"><?=$bnReceipt->getData('note')?></td>
                </tr>
              </tbody>
            </table>
            <? } ?>
            </td>
            <td width="20"></td>
            <td width="300" align="right">
            <table border="0" cellspacing="0" cellpadding="3">
              <tbody>
                <tr>
                  <td style="font-size: 140%">〒141-0022</td>
                </tr>
                <tr>
                  <td style="font-size: 140%">東京都品川区東五反田2-3-1</td>
                </tr>
                <tr>
                  <td style="font-size: 140%">株式会社オートコミュニケーションズ</td>
                </tr>
                <tr>
                  <td style="font-size: 140%">取扱店・担当者</td>
                </tr>
                <tr>
                  <td style="font-size: 140%">〒<?=$bnStore->getData('zip_code')?></td>
                </tr>
                <tr>
                  <td style="font-size: 140%"><?=$bnStore->getData('address_1')?></td>
                </tr>
                <tr>
                  <td style="font-size: 140%"><?=$bnStore->getData('address_2')?></td>
                </tr>
                <tr>
                  <td style="font-size: 140%"><?=$bnStore->getData('name')?></td>
                </tr>
                <tr>
                  <td style="font-size: 140%">TEL:<?=$bnStore->getData('tel')?></td>
                </tr>
                <tr>
                  <td style="font-size: 140%"></td>
                </tr>
                <tr>
                  <td style="font-size: 140%"><?=$bnCharge->getData('name')?><? if (confirm::checkComp($stub, 0)) { ?>　　　　印<? } ?></td>
                </tr>
              </tbody>
            </table>
            </td>
          </tr>
        </tbody>
      </table>
      </td>
    </tr>
    <tr>
      <td height="40"></td>
    </tr>
    <tr>
      <td style="font-size: 140%">任意保険については、保険会社の正規の領収証が届いた際無効となります。</td>
    </tr>
  </tbody>
</table>

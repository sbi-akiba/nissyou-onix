<nav class="navbar navbar-inverse ">
  <div class="container-fluid">

    <div class="navbar-header">
      <a class="navbar-brand" href="/receipt/search/index.php">領収書システム</a>
    </div>

    <div class="collapse navbar-collapse" id="nav_target">
      <ul class="nav navbar-nav">
          <li <?if (strpos($_SERVER['REQUEST_URI'], "/receipt/") === 0) {?>class="active"<?}?>><a href="/receipt/search/index.php">領収書管理</a></li>
          <li <?if (strpos($_SERVER['REQUEST_URI'], "/store/") === 0) {?>class="active"<?}?>><a href="/store/search/index.php">店舗管理</a></li>
          <li <?if (strpos($_SERVER['REQUEST_URI'], "/charge/") === 0) {?>class="active"<?}?>><a href="/charge/search/index.php">担当者管理</a></li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
          <li class="navbar-text">店舗名： <span id="store-name"><?=$session->getData('store_name')?></span></li>
          <li class="navbar-text">担当者： <span id="charge-name"><?=$session->getData('name')?></span></li>
          <li><a href="/logout.php">ログアウト</a></li>
      </ul>
    </div>

  </div>
</nav>

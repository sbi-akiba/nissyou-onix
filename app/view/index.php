<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>領収書システム</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<style type="text/css">
body {
  padding-top: 200px;
  padding-bottom: 200px;
}

.form-signin {
  max-width: 400px;
  padding: 15px;
  margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}
</style>

</head>
<body>
    <div class="container">

        <form class="form-signin" action="login.php" method="post">
            <? if (confirm::checkInput($message)) { ?><div class="alert alert-danger"><?=convert::unescapeLine($message)?></div><? } ?>
            <label for="inputEmail" class="sr-only">ログインID</label>
            <input type="email" id="inputEmail" class="form-control" placeholder="ログインID" name="mail" value="<?=$bnCharge->getData('mail')?>" autofocus>
            <label for="inputPassword" class="sr-only">パスワード</label>
            <input type="password" id="inputPassword" class="form-control" name="password" value="<?=$bnCharge->getData('password')?>" placeholder="パスワード">
            <button class="btn btn-lg btn-primary btn-block" type="submit">ログイン</button>
        </form>
    </div>
</body>
</html>

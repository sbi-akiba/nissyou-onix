<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>領収書システム</title>
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/bootstrap-datepicker3.min.css" rel="stylesheet">
<link href="/css/receipt.css" rel="stylesheet">

<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-datepicker.min.js"></script>
<script src="/js/bootstrap-datepicker.ja.min.js" charset="UTF-8"></script>
<script src="/js/charge.js?<?=VERSION?>" charset="UTF-8"></script>
</head>
<body>

<?require_once(DIR_VIEW . "include/header.php")?>

<div class="container-fluid">

  <div class="page-header" style="margin-top:-20px;padding-bottom:0px;">
    <h1>担当者管理</h1>
  </div>

    <!-- 検索フォーム -->
    <div class="table-responsive">
        <form action="index.php" method="get" accept-charset="utf-8" class="form-horizontal" id="form-search">
        <input type="hidden" name="page" value="1">
        </form>
    </div>

<div id="top-message"><? if (confirm::checkInput($message)) { ?><div class="alert alert-danger"><?=convert::unescapeLine($message)?></div><? } ?></div>

<? if (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) { ?>
<div class="form-inline row" style="padding-bottom:30px;">
  <div class="form-group col-sm-8">
    <button type="button" class="btn btn-primary regist-modal">新規登録</button>&emsp;
  </div>
</div>
<? } ?>

<div class="table-responsive">
<table class="table table-striped">
    <thead>
        <tr>
            <th>店舗</th>
            <th>担当者名</th>
            <th>メールアドレス</th>
            <? if (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) { ?>
            <th>権限</th>
            <th>ログイン制限</th>
            <? } ?>
            <th>登録日</th>
            <th>更新日</th>
            <? if (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) { ?>
            <th>操作</th>
            <? } ?>
        </tr>
    </thead>
    <tbody>
<? if (count($arySearch) <= 0) { ?>
        <tr>
          <td colspan="<?=(!confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) ? "8" : "5" ?>" align="center">担当者情報はありません。</td>
        </tr>
<? } else {
    while(list($index, $data) = each($arySearch)) {
?>
        <tr>
            <td><?=$data['store_name']?></td>
            <td><?=$data['name']?></td>
            <td><?=$data['mail']?></td>
            <? if (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) { ?>
            <td><span class="label label-info" style="font-size: 12px;"><?=$data['type_auth_name']?></span></td>
            <td><span class="label label-danger" style="font-size: 12px;"><? if (confirm::checkComp(LOGIN_LOCK, $data['flug_login_limit'])) { ?>ロック<? } ?></span></td>
            <? } ?>
            <td><?=convert::dateFormat($data['date_regist'])?></td>
            <td><?=convert::dateFormat($data['date_update'])?></td>
            <? if (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) { ?>
            <td>
                <button type="button" class="btn btn-primary btn-xs edit-modal" data-id="<?=$data['id']?>">編集</button>
            </td>
            <? } ?>
        </tr>
    <? } ?>
<? } ?>


    </tbody>
</table>
</div>

<?
    $count = count($arySearch);

    if ($count > 0) {
        $nowPage = $bnSearch->getData('page');
        $totalPage = ceil($intCount / $ini_array["store_search_max"]);
        $startPage = html::pageStart($nowPage, $totalPage, $ini_array["store_search_count"]);
        $endPage = html::pageEnd($nowPage, $totalPage, $ini_array["store_search_count"]);
        $startCount = (($nowPage - 1) * $ini_array["store_search_max"]) + 1;
        $endCount = $startCount + $count - 1;
        $nextCount = (($nowPage + 1) == $totalPage) ? $intCount - ($nowPage * $ini_array["store_search_max"]) : $ini_array["store_search_max"];
        $backCount = $ini_array["store_search_max"];
    }
?>

<nav>
    <ul class="pagination">

<? if ($count > 0) { ?>
    <? if ($nowPage > 1) { ?>
        <li><a href="#" class="page-submit" id="<?=$nowPage - 1?>" aria-label="前のページへ"><span aria-hidden="true">«</span></a></li>
    <? } ?>

    <? if ($endPage != 1) { ?>
        <? for ($cntPage = $startPage; $cntPage <= $endPage; $cntPage++) { ?>
            <? if ($cntPage == $nowPage) { ?>
        <li class="active"><a href="#" id="<?=$nowPage - 1?>"><?=$nowPage?></a></li>
            <?} else {?>
        <li><a href="#" class="page-submit" id="<?=$cntPage?>"><?=$cntPage?></a></li>
            <? } ?>
        <? } ?>
    <? } ?>

    <? if($nowPage < $endPage) { ?>
        <li><a href="#" class="page-submit" id="<?=$nowPage + 1?>" aria-label="次のページへ"><span aria-hidden="true">»</span></a></li>
    <? } ?>
<? } ?>
    </ul>
</nav>
</div>

<? if (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) { ?>
<div class="modal" id="myModalRegist">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel1">新規登録</h4>
            </div>
            <div class="modal-body">
            <div id="regist-message"></div>
            <form class="form-horizontal" id="form-regist">
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">店舗名</label>
                <div class="col-sm-8">
                  <?=html::pulldown("id_store", $aryStore, "", "class=\"form-control\" id=\"id_store\"")?>
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">担当者名</label>
                <div class="col-sm-8">
                  <input type="text" name="name" id="name" class="form-control" style="ime-mode:active;">
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">メールアドレス</label>
                <div class="col-sm-8">
                  <input type="text" name="mail" id="mail" class="form-control" style="ime-mode:inactive;">
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">パスワード</label>
                <div class="col-sm-8">
                  <input type="password" name="password" id="password" class="form-control" style="ime-mode:inactive;">
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">権限</label>
                <div class="col-sm-8">
                  <?=html::pulldown("id_type_auth", $aryTypeAuth, "", "class=\"form-control\" id=\"id_type_auth\"")?>
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">ログイン制限</label>
                <div class="col-sm-8">
                <label class="checkbox-inline"><input type="checkbox" name="flug_login_limit" id="flug_login_limit" value="1"> ロック</label>
                </div>
              </div>

            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary regist-submit">保存</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModalEdit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel1">編集</h4>
            </div>
            <div class="modal-body">
            <div id="edit-message"></div>
            <form class="form-horizontal" id="form-edit">
              <input type="hidden" name="id">
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">店舗名</label>
                <div class="col-sm-8">
                  <?=html::pulldown("id_store", $aryStore, "", "class=\"form-control\" id=\"id_store\"")?>
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">担当者名</label>
                <div class="col-sm-8">
                  <input type="text" name="name" id="name" class="form-control" style="ime-mode:active;">
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">メールアドレス</label>
                <div class="col-sm-8">
                  <input type="text" name="mail" id="mail" class="form-control" style="ime-mode:inactive;">
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">パスワード</label>
                <div class="col-sm-8">
                  <input type="password" name="password" id="password" class="form-control" style="ime-mode:inactive;">
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">権限</label>
                <div class="col-sm-8">
                  <?=html::pulldown("id_type_auth", $aryTypeAuth, "", "class=\"form-control\" id=\"id_type_auth\"")?>
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">ログイン制限</label>
                <div class="col-sm-8">
                <label class="checkbox-inline"><input type="checkbox" name="flug_login_limit" id="flug_login_limit" value="1"> ロック</label>
                </div>
              </div>

            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary edit-submit">保存</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
            </div>
        </div>
    </div>
</div>
<? } ?>

</body>
</html>

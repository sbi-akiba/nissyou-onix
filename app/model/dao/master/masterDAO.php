<?php
require_once("web.php");
require_once(DIR_CLASS . "common/db.php");
require_once(DIR_CLASS . "common/confirm.php");

class masterDAO {
    var $master;

    // マスタ情報チェック
    function checkMaster($masterName, $item, $key = "id") {
        try {
            // 初期処理
            $cntResult = "";
            $dbControl = new db();
            $dbControl->db_connect();
            $strSql = "SELECT * FROM {$masterName} WHERE {$key} = {$item}";

            // SQL実行
            $dbControl->prepare($strSql);
            $dbControl->execute();

            $cntResult = $dbControl->getRowCount();
            $dbControl->db_close();

            return $cntResult;

        } catch (PDOException $e) {
            throw $e;
        }
    }
}
?>

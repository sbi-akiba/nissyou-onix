<?php
require_once("web.php");
require_once(DIR_CLASS . "common/db.php");
require_once(DIR_CLASS . "common/confirm.php");

class searchDAO {

    // 全領収書取得
    function getAll($bnSearch) {
        // 初期処理
        $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
        $aryItem = array();
        $strCondition = "";
        $strOrder = "";
        $limit = "";

        // 条件設定
        $strCondition = $this->getCondition($aryItem, $bnSearch);

        // 並び順設定
        $strOrder = "order by t_receipt.date_issue IS NULL desc, t_receipt.receipt_number desc, t_store.store_number, t_receipt.id_charge, t_receipt.date_update desc ";

        // リミット設定
        if (confirm::checkInput($bnSearch->getData('page'))) {
            $limit = "limit " . ($bnSearch->getData('page') - 1) * $ini_array["receipt_search_max"] . ", " . $ini_array["receipt_search_max"];
        }

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select " . 
                  "t_receipt.id, t_receipt.flug_enable, t_receipt.receipt_number, t_receipt.flug_print, t_receipt.id_store, " . 
                  "t_receipt.store_name, t_store.store_number, t_store.name as store_name_2, t_receipt.id_charge, t_receipt.charge_name as charge_name, " . 
                  "t_charge.name as charge_name_2, t_receipt.customer, t_receipt.id_type_honorific, m_type_honorific.value as honorific_value, t_receipt.amount, " . 
                  "t_receipt.tax, t_receipt.proviso, t_receipt.note, DATE_FORMAT(t_receipt.date_issue, '%Y年%m月%d日') as date_issue, t_receipt.date_regist, t_receipt.date_update " . 
                  "from (((t_receipt inner join t_store on t_receipt.id_store = t_store.id) " . 
                  "inner join t_charge on t_receipt.id_charge = t_charge.id) " . 
                  "inner join m_type_honorific on t_receipt.id_type_honorific = m_type_honorific.id) " . 
                  $strCondition . $strOrder . $limit;

        // SQL実行
        $dbControl->prepare($strSql);
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 1, $value);
        $dbControl->execute();

        $aryTemp = $dbControl->fetchAll();
        $dbControl->db_close();

        return $aryTemp;
    }

    // 全領収書取得
    function getAllCsv($bnSearch) {
        // 初期処理
        $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
        $aryItem = array();
        $strCondition = "";
        $strOrder = "";
        $limit = "";

        // 条件設定
        $strCondition = $this->getCondition($aryItem, $bnSearch);

        // 並び順設定
        $strOrder = "order by t_receipt.date_issue IS NULL desc, t_receipt.date_issue desc ";

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select " . 
                  "t_receipt.receipt_number, DATE_FORMAT(t_receipt.date_issue, '%Y年%m月%d日') as date_issue, t_store.name as store_name, t_charge.name as charge_name, " . 
                  "t_receipt.customer, t_receipt.amount, t_receipt.proviso, t_receipt.note, (CASE WHEN t_receipt.flug_print = 1 THEN '済' ELSE '' END) as print_status " . 
                  "from (((t_receipt inner join t_store on t_receipt.id_store = t_store.id) " . 
                  "inner join t_charge on t_receipt.id_charge = t_charge.id) " . 
                  "inner join m_type_honorific on t_receipt.id_type_honorific = m_type_honorific.id) " . 
                  $strCondition . $strOrder;

        // SQL実行
        $dbControl->prepare($strSql);
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 1, $value);
        $dbControl->execute();

        $aryTemp = $dbControl->fetchAll();
        $dbControl->db_close();


        return $aryTemp;
    }

    // 領収書数取得
    function getAllCount($bnSearch) {
        // 初期処理
        $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
        $aryItem = array();
        $strCondition = "";

        // 条件設定
        $strCondition = $this->getCondition($aryItem, $bnSearch);

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select * from t_receipt " . $strCondition;

        // SQL実行
        $dbControl->prepare($strSql);
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 1, $value);
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    // 全領収書金額取得
    function getAllAmount($bnSearch) {
        // 初期処理
        $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
        $aryItem = array();
        $strCondition = "";
        $intAmount = 0;

        // 条件設定
        $strCondition = $this->getCondition($aryItem, $bnSearch);

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select SUM(amount) as all_amount from t_receipt " . $strCondition;

        // SQL実行
        $dbControl->prepare($strSql);
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 1, $value);
        $dbControl->execute();

        $aryTemp = $dbControl->fetchAll();
        $dbControl->db_close();

        if ((is_array($aryTemp)) && (count($aryTemp) == 1)) {
            list($index, $aryReceipt) = each($aryTemp);
            $intAmount = $aryReceipt['all_amount'];
        }

        return $intAmount;
    }

    function getCondition(&$aryItem, $bnSearch) {
        // 初期処理
        $strCondition = "";
        $session = new session();

        // 印刷状態
        $strCondition .= "and t_receipt.flug_enable = 1 ";

        // 権限設定
        if (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) {
            // 店舗名設定
            if (confirm::checkInput($bnSearch->getData('id_store'))) {
                $strCondition .= "and t_receipt.id_store = ? ";
                array_push($aryItem, $bnSearch->getData('id_store'));
            }

            // 担当者設定
            if (confirm::checkInput($bnSearch->getData('id_charge'))) {
                $strCondition .= "and t_receipt.id_charge = ? ";
                array_push($aryItem, $bnSearch->getData('id_charge'));
            }

        } else {
            // 店舗名設定
            $strCondition .= "and t_receipt.id_store = ? ";
            array_push($aryItem, $session->getData('id_store'));

            // 担当者設定
            if (confirm::checkInput($bnSearch->getData('id_charge'))) {
                $strCondition .= "and t_receipt.id_charge = ? ";
                array_push($aryItem, $bnSearch->getData('id_charge'));
            }

        }

        // 印刷設定
        if (confirm::checkInput($bnSearch->getData('flug_print'))) {
            $strCondition .= "and t_receipt.flug_print = ? ";
            array_push($aryItem, $bnSearch->getData('flug_print'));
        }

        // 発行日設定
        if (confirm::checkInput($bnSearch->getData('date_issue_01'))) {
            $date_issue_01 = date("Y-m-d 00:00:00", strtotime($bnSearch->getData('date_issue_01')));
            $strCondition .= "and t_receipt.date_issue >= ? ";
            array_push($aryItem, $date_issue_01);
        }

        if (confirm::checkInput($bnSearch->getData('date_issue_02'))) {
            $date_issue_02 = date("Y-m-d 23:59:59", strtotime($bnSearch->getData('date_issue_02')));
            $strCondition .= "and t_receipt.date_issue <= ? ";
            array_push($aryItem, $date_issue_02);
        }

        // 印刷設定
        if (confirm::checkInput($bnSearch->getData('flug_print'))) {
            $strCondition .= "and t_receipt.flug_print = ? ";
            array_push($aryItem, $bnSearch->getData('flug_print'));
        }

        // お客さま名設定
        if (confirm::checkInput($bnSearch->getData('customer'))) {
            $strCondition .= "and t_receipt.customer like ? ";
            array_push($aryItem, sprintf('%%%s%%', addcslashes($bnSearch->getData('customer'), '\_%')));
        }

        // 但し書き設定
        if (confirm::checkInput($bnSearch->getData('proviso'))) {
            $strCondition .= "and t_receipt.proviso like ? ";
            array_push($aryItem, sprintf('%%%s%%', addcslashes($bnSearch->getData('proviso'), '\_%')));
        }

        // 金額設定
        if (confirm::checkInput($bnSearch->getData('amount'))) {
            $strCondition .= "and t_receipt.amount like ? ";
            array_push($aryItem, sprintf('%%%s%%', addcslashes($bnSearch->getData('amount'), '\_%')));
        }

        // 備考設定
        if (confirm::checkInput($bnSearch->getData('note'))) {
            $strCondition .= "and t_receipt.note like ? ";
            array_push($aryItem, sprintf('%%%s%%', addcslashes($bnSearch->getData('note'), '\_%')));
        }

        $strCondition = substr($strCondition, mb_strlen("and "));
        return ($strCondition != "") ? "where " . $strCondition : "";
    }
}
?>

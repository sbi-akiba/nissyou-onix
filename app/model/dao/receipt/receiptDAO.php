<?php
require_once("web.php");
require_once(DIR_CLASS . "common/db.php");
require_once(DIR_CLASS . "bean/receipt/receiptBean.php");

class receiptDAO {

    // 領収書取得
    function getOne($bnReceipt) {
        // 初期処理
        $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
        $bnReceiptTemp = new receiptBean();
        $aryItem = array();
        $strCondition = "";

        // 条件設定
        $strCondition = $this->getCondition($aryItem, $bnReceipt);

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select " . 
                  "t_receipt.id, t_receipt.flug_enable, t_receipt.receipt_number, t_receipt.flug_print, t_receipt.id_store, " . 
                  "t_receipt.store_name, t_store.name as store_name_2, t_receipt.id_charge, t_receipt.charge_name, t_charge.name as charge_name2, " . 
                  "t_receipt.customer, t_receipt.id_type_honorific, m_type_honorific.value as honorific_value, t_receipt.amount, t_receipt.tax, " . 
                  "t_receipt.proviso, t_receipt.note, DATE_FORMAT(t_receipt.date_issue, '%Y年%m月%d日') as date_issue, t_receipt.date_regist, t_receipt.date_update " . 
                  "from (((t_receipt inner join t_store on t_receipt.id_store = t_store.id) " . 
                  "inner join t_charge on t_receipt.id_charge = t_charge.id) " . 
                  "inner join m_type_honorific on t_receipt.id_type_honorific = m_type_honorific.id) " . 
                  $strCondition;

        // SQL実行
        $dbControl->prepare($strSql);
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 1, $value);
        $dbControl->execute();

        $aryTemp = $dbControl->fetchAll();
        $dbControl->db_close();

        if ((is_array($aryTemp)) && (count($aryTemp) == 1)) {
            list($index, $aryReceipt) = each($aryTemp);
            $bnReceiptTemp->setDataAll($aryReceipt);
        }

        return $bnReceiptTemp;
    }

    // 領収書登録
    function setOne($bnReceipt) {
        // 初期処理
        $dbControl = new db();
        $dbControl->db_connect();
        $session = new session();

        // SQL文生成
        $strSql = "INSERT INTO t_receipt (" . 
                 "id, flug_enable, receipt_number, flug_print, id_store, store_name, " . 
                 "id_charge, charge_name, customer, id_type_honorific, amount, tax, " . 
                 "proviso, note, date_issue, date_regist, date_update) " . 
                 "VALUES (NULL, 1, NULL, 0, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, null, now(), now())";

        // SQL実行
        $dbControl->prepare($strSql);
        $dbControl->setValue(1, $session->getData('id_store'));
        $dbControl->setValue(2, $session->getData('store_name'));
        $dbControl->setValue(3, $session->getData('id'));
        $dbControl->setValue(4, $session->getData('name'));
        $dbControl->setValue(5, $bnReceipt->getData('customer'));
        $dbControl->setValue(6, $bnReceipt->getData('id_type_honorific'));
        $dbControl->setValue(7, $bnReceipt->getData('amount'));
        $dbControl->setValue(8, $bnReceipt->getData('tax'));
        $dbControl->setValue(9, $bnReceipt->getData('proviso'));
        $dbControl->setValue(10, $bnReceipt->getData('note'));
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    // 領収書編集
    function chgOne($bnReceipt) {
        // 初期処理
        $aryItem = array();
        $strCondition = "";
        $strAddCondition = "";
        $session = new session();

        // 条件設定
        $strAddCondition = "and t_receipt.flug_print = 0 ";
        $strCondition = $this->getCondition($aryItem, $bnReceipt, $strAddCondition);

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "UPDATE t_receipt SET " . 
                  "id_store = ?, store_name = ?, id_charge = ?, charge_name = ?, customer = ?, id_type_honorific = ?, amount = ?, tax = ?, proviso = ?, note = ?, date_update = now() " . 
                  $strCondition;

        // SQL実行
        $dbControl->prepare($strSql);
        $dbControl->setValue(1, $session->getData('id_store'));
        $dbControl->setValue(2, $session->getData('store_name'));
        $dbControl->setValue(3, $session->getData('id'));
        $dbControl->setValue(4, $session->getData('name'));
        $dbControl->setValue(5, $bnReceipt->getData('customer'));
        $dbControl->setValue(6, $bnReceipt->getData('id_type_honorific'));
        $dbControl->setValue(7, $bnReceipt->getData('amount'));
        $dbControl->setValue(8, $bnReceipt->getData('tax'));
        $dbControl->setValue(9, $bnReceipt->getData('proviso'));
        $dbControl->setValue(10, $bnReceipt->getData('note'));
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 11, $value);
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    // 領収書編集
    function chgOne2($bnReceipt) {
        // 初期処理
        $aryItem = array();
        $strCondition = "";

        // 条件設定
        $strCondition = $this->getCondition($aryItem, $bnReceipt);

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "UPDATE t_receipt SET " . 
                  "customer = ?, id_type_honorific = ?, amount = ?, tax = ?, proviso = ?, note = ?, date_update = now() " . 
                  $strCondition;

        // SQL実行
        $dbControl->prepare($strSql);
        $dbControl->setValue(1, $bnReceipt->getData('customer'));
        $dbControl->setValue(2, $bnReceipt->getData('id_type_honorific'));
        $dbControl->setValue(3, $bnReceipt->getData('amount'));
        $dbControl->setValue(4, $bnReceipt->getData('tax'));
        $dbControl->setValue(5, $bnReceipt->getData('proviso'));
        $dbControl->setValue(6, $bnReceipt->getData('note'));
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 7, $value);
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    // 領収書印刷設定
    function setPrint($bnReceipt) {
        // 初期処理
        $aryItem = array();
        $strCondition = "";
        $strAddCondition = "";
        $session = new session();

        // 条件設定
        $strAddCondition = "and t_receipt.flug_print = 0 ";
        $strCondition = $this->getCondition($aryItem, $bnReceipt, $strAddCondition);

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "UPDATE t_receipt SET receipt_number = ?, flug_print = 1, id_store = ?, store_name = ?, id_charge = ?, charge_name = ?, date_issue = now(), date_update = now() " . 
                  $strCondition;

        // SQL実行
        $dbControl->prepare($strSql);
        $dbControl->setValue(1, $bnReceipt->getData('receipt_number'));
        $dbControl->setValue(2, $session->getData('id_store'));
        $dbControl->setValue(3, $session->getData('store_name'));
        $dbControl->setValue(4, $session->getData('id'));
        $dbControl->setValue(5, $session->getData('name'));

        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 6, $value);
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    // 領収書削除
    function delOne($bnReceipt) {
        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "UPDATE t_receipt SET flug_enable = 0, date_update = now() where flug_print = 0 AND id = ?";

        // SQL実行
        $dbControl->prepare($strSql);
        $dbControl->setValue(1, $bnReceipt->getData('id'));
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    // 印刷状態確認
    function chkPrint($strId) {
        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select * from t_receipt where id = ? AND flug_print = 1";

        // SQL実行
        $dbControl->prepare($strSql);
        $dbControl->setValue(1, $strId);
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    // 削除状態確認
    function chkDelete($strId) {
        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select * from t_receipt where id = ? AND flug_enable = 0";

        // SQL実行
        $dbControl->prepare($strSql);
        $dbControl->setValue(1, $strId);
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    function getCondition(&$aryItem, $bnReceipt, $strAddCondition = "") {
        // 初期処理
        $strCondition = "";
        $session = new session();

        // 領収書状態
        $strCondition .= "and t_receipt.flug_enable = 1 ";

        // 管理ID
        $strCondition .= "and t_receipt.id = ? ";
        array_push($aryItem, $bnReceipt->getData('id'));

        // 権限設定
        if (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) {
        } else {
            $strCondition .= "and t_receipt.id_store = ? ";
            array_push($aryItem, $session->getData('id_store'));
        }

        // 追加条件
        $strCondition .= $strAddCondition;

        $strCondition = substr($strCondition, mb_strlen("and "));
        return ($strCondition != "") ? "where " . $strCondition : "";
    }
}
?>

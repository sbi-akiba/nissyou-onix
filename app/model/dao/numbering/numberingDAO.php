<?php
require_once("web.php");
require_once(DIR_CLASS . "common/db.php");
require_once(DIR_CLASS . "common/convert.php");
require_once(DIR_CLASS . "bean/numbering/numberingBean.php");

class numberingDAO {

    // 採番設定
    function setOne($bnNumbering) {

        $intNumber = "";
        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "INSERT INTO t_numbering (" . 
                 "id, id_store, year, number, date_regist, date_update) " . 
                 "select ifnull((select max(id) from t_numbering) + 1, 1),  " . 
                 "?, " . 
                 "'" . date("Y") . "', " . 
                 "LAST_INSERT_ID(1), " . 
                 "now(), " . 
                 "now() " . 
                 "ON DUPLICATE KEY UPDATE " . 
                 "number = LAST_INSERT_ID(number + 1), " . 
                 "date_update = now() ";

        // SQL実行
        $dbControl->prepare($strSql);
        $dbControl->setValue(1, $bnNumbering->getData("id_store"));
        $dbControl->execute();

        // SQL実行
        $dbControl->query("select LAST_INSERT_ID() as number");
        $aryTemp = $dbControl->fetchAll();

        // ID取得
        list($index, $aryData) = each($aryTemp);
        $intNumber = $aryData['number'];

        $dbControl->db_close();
        return $intNumber;
    }
}
?>

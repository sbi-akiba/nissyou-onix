<?php
require_once("web.php");
require_once(DIR_CLASS . "common/db.php");

class typeHonorificDAO {

    // 敬称区分取得
    function getAllSimple(){
        $dbControl = new db();
        $dbControl->db_connect();

        // SQL実行
        $dbControl->query("select * from m_type_honorific order by sort");
        $aryTemp = $dbControl->fetchAll();
        $dbControl->db_close();

        return $aryTemp;
    }
}
?>

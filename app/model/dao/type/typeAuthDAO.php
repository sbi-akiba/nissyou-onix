<?php
require_once("web.php");
require_once(DIR_CLASS . "common/db.php");

class typeAuthDAO {

    // 権限区分取得
    function getAllSimple(){
        $dbControl = new db();
        $dbControl->db_connect();

        // SQL実行
        $dbControl->query("select * from m_type_auth order by sort");
        $aryTemp = $dbControl->fetchAll();
        $dbControl->db_close();

        return $aryTemp;
    }
}
?>

<?php
require_once("web.php");
require_once(DIR_CLASS . "common/db.php");
require_once(DIR_CLASS . "common/confirm.php");

class searchDAO {

    // 全担当者取得
    function getAll($bnSearch) {
        // 初期処理
        $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
        $aryItem = array();
        $strCondition = "";
        $strOrder = "";
        $limit = "";

        // 条件設定
        $strCondition = $this->getCondition($aryItem, $bnSearch);

        // 並び順設定
        $strOrder = "order by id_store asc, id ";

        // リミット設定
        if (confirm::checkInput($bnSearch->getData('page'))) {
            $limit = "limit " . ($bnSearch->getData('page') - 1) * $ini_array["charge_search_max"] . ", " . $ini_array["charge_search_max"];
        }

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select t_charge.*, t_store.name as store_name, t_store.store_number, m_type_auth.value as type_auth_name " . 
                  "from ((t_charge " . 
                  "inner join t_store on t_charge.id_store = t_store.id) " . 
                  "left join m_type_auth on t_charge.id_type_auth = m_type_auth.id) " . 
                  $strCondition . $strOrder . $limit;

        // SQL実行
        $dbControl->prepare($strSql);
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 1, $value);
        $dbControl->execute();

        $aryTemp = $dbControl->fetchAll();
        $dbControl->db_close();

        return $aryTemp;
    }

    // 担当者数取得
    function getAllCount($bnSearch) {
        // 初期処理
        $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
        $aryItem = array();
        $strCondition = "";

        // 条件設定
        $strCondition = $this->getCondition($aryItem, $bnSearch);

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select * from t_charge " . $strCondition;

        // SQL実行
        $dbControl->prepare($strSql);
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 1, $value);
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    function getCondition(&$aryItem, $bnSearch) {
        // 初期処理
        $strCondition = "";
        $session = new session();

        // 担当者状態
        $strCondition .= "and t_charge.flug_enable = 1 ";

        // 権限設定
        if (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) {
        } else {
            $strCondition .= "and t_charge.id = ? ";
            array_push($aryItem, $session->getData('id'));
        }

        $strCondition = substr($strCondition, mb_strlen("and "));
        return ($strCondition != "") ? "where " . $strCondition : "";
    }
}
?>

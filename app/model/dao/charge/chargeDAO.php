<?php
require_once("web.php");
require_once(DIR_CLASS . "common/db.php");
require_once(DIR_CLASS . "common/confirm.php");
require_once(DIR_CLASS . "common/convert.php");
require_once(DIR_CLASS . "bean/charge/chargeBean.php");

class chargeDAO {

    // 担当者情報全取得
    function getAllSimple() {
        // 初期処理
        $aryItem = array();
        $strCondition = "";
        $order = "";

        // 条件設定
        $strCondition = $this->getCondition($aryItem, new chargeBean());

        // 並び順設定
        $order = "order by id_store, id_type_auth ";

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select id, name as value from t_charge " . $strCondition . $order;

        // SQL実行
        $dbControl->prepare($strSql);
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 1, $value);
        $dbControl->execute();

        $aryTemp = $dbControl->fetchAll();
        $dbControl->db_close();

        return $aryTemp;
    }

    // 担当者情報取得
    function getLogin($bnCharge) {
        // 初期処理
        $bnChargeTemp = new chargeBean();

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $sql = "select t_charge.*, t_store.name as store_name, t_store.store_number, m_type_auth.value as type_auth_name " . 
                 "from ((t_charge " . 
                 "inner join t_store on t_charge.id_store = t_store.id) " . 
                 "left join m_type_auth on t_charge.id_type_auth = m_type_auth.id) " . 
                 "where t_charge.flug_enable = 1 and t_charge.mail = ? ";

        $dbControl->prepare($sql);
        $dbControl->setValue(1, $bnCharge->getData('mail'));
        $dbControl->execute();
        $aryTemp = $dbControl->fetchAll();
        $dbControl->db_close();

        if ((is_array($aryTemp)) && (count($aryTemp) == 1)) {
            list($index, $aryCharge) = each($aryTemp);
            $bnChargeTemp->setDataAll($aryCharge);
        }

        return $bnChargeTemp;
    }

    // 担当者情報取得
    function getOne($bnCharge) {
        // 初期処理
        $strCondition = "";
        $bnChargeTemp = new chargeBean();
        $aryItem = array();

        // 条件設定
        $strCondition = $this->getCondition($aryItem, $bnCharge);

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select t_charge.*, t_store.name as store_name, t_store.store_number, m_type_auth.value as type_auth_name " . 
                  "from ((t_charge " . 
                  "inner join t_store on t_charge.id_store = t_store.id) " . 
                  "left join m_type_auth on t_charge.id_type_auth = m_type_auth.id) " . 
                  $strCondition;

        // SQL実行
        $dbControl->prepare($strSql);
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 1, $value);
        $dbControl->execute();

        $aryTemp = $dbControl->fetchAll();
        $dbControl->db_close();

        if ((is_array($aryTemp)) && (count($aryTemp) == 1)) {
            list($index, $aryCharge) = each($aryTemp);
            $bnChargeTemp->setDataAll($aryCharge);
        }

        return $bnChargeTemp;
    }

    // アカウント存在確認
    function chkAcount($strId, $strAcount) {
        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select * from t_charge where mail = ? ";
        $strSql .= (confirm::checkInput($strId)) ? "and id != ?" : "";

        // SQL実行
        $dbControl->prepare($strSql);
        $dbControl->setValue(1, $strAcount);
        if (confirm::checkInput($strId)) $dbControl->setValue(2, $strId);
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    // 担当者登録
    function setOne($bnCharge) {
        // 初期処理
        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "INSERT INTO t_charge (" . 
                  "id, id_store, flug_enable, name, mail, " . 
                  "password, id_type_auth, flug_login_limit, login_limit_count, date_regist, date_update) " . 
                  "VALUES (NULL, ?, 1, ?, ?, md5(?), ?, ?, ?, now(), now())";

        // SQL実行
        $dbControl->prepare($strSql);
        $dbControl->setValue(1, $bnCharge->getData('id_store'));
        $dbControl->setValue(2, $bnCharge->getData('name'));
        $dbControl->setValue(3, $bnCharge->getData('mail'));
        $dbControl->setValue(4, $bnCharge->getData('password'));
        $dbControl->setValue(5, $bnCharge->getData('id_type_auth'));
        $dbControl->setValue(6, $bnCharge->getData('flug_login_limit'));
        $dbControl->setValue(7, $bnCharge->getData('login_limit_count'));
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    // 担当者編集
    function chgOne($bnCharge) {
        // 初期処理
        $aryItem = array();
        $session = new session();
        $intItem = 0;
        $strCondition = "";

        // 条件設定
        $strCondition = $this->getCondition($aryItem, $bnCharge);

        $dbControl = new db();
        $dbControl->db_connect();

        $strIdStore = (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) ? "id_store = ?, " : "";
        $strPassword = (confirm::checkInput($bnCharge->getData('password'))) ? "password = md5(?), " : "";
        $strIdTypeAuth = (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) ? "id_type_auth = ?, " : "";
        $strFlugLoginLimit = (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) ? "flug_login_limit = ?, " : "";
        $strLoginLimitCount = (confirm::checkComp($bnCharge->getData('flug_login_limit'), LOGIN_UNLOCK)) ? "login_limit_count = 0, " : "";

        // SQL文生成
        $strSql = "UPDATE t_charge SET " . 
                  "name = ?, " . 
                  "mail = ?, " . 
                  $strIdStore . 
                  $strPassword . 
                  $strIdTypeAuth . 
                  $strFlugLoginLimit . 
                  $strLoginLimitCount . 
                  "date_update = now() " . 
                  $strCondition;

        // SQL実行
        $dbControl->prepare($strSql);
        $dbControl->setValue(++$intItem, $bnCharge->getData('name'));
        $dbControl->setValue(++$intItem, $bnCharge->getData('mail'));
        if (confirm::checkInput($strIdStore)) $dbControl->setValue(++$intItem, $bnCharge->getData('id_store'));
        if (confirm::checkInput($strPassword)) $dbControl->setValue(++$intItem, $bnCharge->getData('password'));
        if (confirm::checkInput($strIdTypeAuth)) $dbControl->setValue(++$intItem, $bnCharge->getData('id_type_auth'));
        if (confirm::checkInput($strFlugLoginLimit)) $dbControl->setValue(++$intItem, $bnCharge->getData('flug_login_limit'));
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + $intItem + 1, $value);
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    // 不適合回数設定
    function addMismatch($bnCharge) {
        // 初期処理
        $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");

        $dbControl = new db();
        $dbControl->db_connect();

        $sql = "UPDATE t_charge SET login_limit_count = login_limit_count + 1, flug_login_limit = CASE WHEN login_limit_count >= " . $ini_array['login_limit_count'] . " THEN 1 ELSE 0 END WHERE id = ?";

        $dbControl->prepare($sql);
        $dbControl->setValue(1, $bnCharge->getData('id'));
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    // 不適合回数リセット
    function delMismatch($bnCharge) {
        // 初期処理
        $dbControl = new db();
        $dbControl->db_connect();

        $sql = "UPDATE t_charge SET login_limit_count = 0, flug_login_limit = 0 WHERE id = ?";;

        $dbControl->prepare($sql);
        $dbControl->setValue(1, $bnCharge->getData('id'));
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    function getCondition(&$aryItem, $bnCharge) {
        // 初期処理
        $strCondition = "";
        $session = new session();

        // 店舗状態
        $strCondition .= "and t_charge.flug_enable = 1 ";

        // 権限設定
        if (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) {
            if (confirm::checkInput($bnCharge->getData('id'))) {
                $strCondition .= "and t_charge.id = ? ";
                array_push($aryItem, $bnCharge->getData('id'));
            }

        } else {
            $strCondition .= "and t_charge.id_store = ? ";
            array_push($aryItem, $session->getData('id_store'));

            if (confirm::checkInput($bnCharge->getData('id'))) {
                $strCondition .= "and t_charge.id = ? ";
                array_push($aryItem, $session->getData('id'));
            }
        }

        $strCondition = substr($strCondition, mb_strlen("and "));
        return ($strCondition != "") ? "where " . $strCondition : "";
    }
}
?>

<?php
require_once("web.php");
require_once(DIR_CLASS . "common/db.php");
require_once(DIR_CLASS . "common/confirm.php");

class searchDAO {

    // 全店舗取得
    function getAll($bnSearch) {
        // 初期処理
        $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
        $aryItem = array();
        $strCondition = "";
        $strOrder = "";
        $limit = "";

        // 条件設定
        $strCondition = $this->getCondition($aryItem, $bnSearch);

        // 並び順設定
        $strOrder = "order by flug_enable desc ";

        // リミット設定
        if (confirm::checkInput($bnSearch->getData('page'))) {
            $limit = "limit " . ($bnSearch->getData('page') - 1) * $ini_array["store_search_max"] . ", " . $ini_array["store_search_max"];
        }

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select * from t_store " . 
                  $strCondition . $strOrder . $limit;

        // SQL実行
        $dbControl->prepare($strSql);
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 1, $value);
        $dbControl->execute();

        $aryTemp = $dbControl->fetchAll();
        $dbControl->db_close();

        return $aryTemp;
    }

    // 店舗数取得
    function getAllCount($bnSearch) {
        // 初期処理
        $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
        $aryItem = array();
        $strCondition = "";

        // 条件設定
        $strCondition = $this->getCondition($aryItem, $bnSearch);

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select * from t_store " . $strCondition;

        // SQL実行
        $dbControl->prepare($strSql);
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 1, $value);
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    function getCondition(&$aryItem, $bnSearch) {
        // 初期処理
        $strCondition = "";
        $session = new session();

        // 店舗状態
        $strCondition .= "and flug_enable = 1 ";

        // 権限設定
        if (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) {
        } else {
            $strCondition .= "and t_store.id = ? ";
            array_push($aryItem, $session->getData('id_store'));
        }

        $strCondition = substr($strCondition, mb_strlen("and "));
        return ($strCondition != "") ? "where " . $strCondition : "";
    }
}
?>

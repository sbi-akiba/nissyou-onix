<?php
require_once("web.php");
require_once(DIR_CLASS . "common/db.php");
require_once(DIR_CLASS . "common/confirm.php");
require_once(DIR_CLASS . "common/convert.php");
require_once(DIR_CLASS . "bean/store/storeBean.php");

class storeDAO {

    // 全店舗情報取得
    function getAllSimple() {
        // 初期処理
        $aryItem = array();
        $strCondition = "";
        $order = "";

        // 条件設定
        $strCondition = $this->getCondition($aryItem, new storeBean());

        // 並び順設定
        $order = "order by store_number ";

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select id, name as value from t_store " . $strCondition . $order;

        // SQL実行
        $dbControl->prepare($strSql);
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 1, $value);
        $dbControl->execute();

        $aryTemp = $dbControl->fetchAll();
        $dbControl->db_close();

        return $aryTemp;
    }

    // 店舗情報取得
    function getOne($bnStore) {
        // 初期処理
        $strCondition = "";
        $bnStoreTemp = new storeBean();
        $aryItem = array();

        // 条件設定
        $strCondition = $this->getCondition($aryItem, $bnStore);

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "select * from t_store " . $strCondition;

        // SQL実行
        $dbControl->prepare($strSql);
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 1, $value);
        $dbControl->execute();

        $aryTemp = $dbControl->fetchAll();
        $dbControl->db_close();

        if ((is_array($aryTemp)) && (count($aryTemp) == 1)) {
            list($index, $aryStore) = each($aryTemp);
            $bnStoreTemp->setDataAll($aryStore);
        }

        return $bnStoreTemp;
    }

    // 店舗登録
    function setOne($bnStore) {
        // 初期処理
        $tmpId = "";

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "INSERT INTO t_store (" . 
                  "id, flug_enable, store_number, name, zip_code, " . 
                  "address_1, address_2, tel, fax, date_regist, date_update) " . 
                  "VALUES (NULL, 1, ?, ?, ?, ?, ?, ?, ?, now(), now())";

        // SQL実行
        $dbControl->prepare($strSql);
        $dbControl->setValue(1, str_pad($bnStore->getData('store_number'), 2, 0, STR_PAD_LEFT));
        $dbControl->setValue(2, $bnStore->getData('name'));
        $dbControl->setValue(3, $bnStore->getData('zip_code'));
        $dbControl->setValue(4, $bnStore->getData('address_1'));
        $dbControl->setValue(5, $bnStore->getData('address_2'));
        $dbControl->setValue(6, $bnStore->getData('tel'));
        $dbControl->setValue(7, $bnStore->getData('fax'));
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    // 店舗編集
    function chgOne($bnStore) {
        // 初期処理
        $aryItem = array();
        $strCondition = "";

        // 条件設定
        $strCondition = $this->getCondition($aryItem, $bnStore);

        $dbControl = new db();
        $dbControl->db_connect();

        // SQL文生成
        $strSql = "UPDATE t_store SET " . 
                  "name = ?, zip_code = ?, address_1 = ?, address_2 = ?, tel = ?, fax = ?, date_update = now() " . 
                  $strCondition;

        // SQL実行
        $dbControl->prepare($strSql);
        $dbControl->setValue(1, $bnStore->getData('name'));
        $dbControl->setValue(2, $bnStore->getData('zip_code'));
        $dbControl->setValue(3, $bnStore->getData('address_1'));
        $dbControl->setValue(4, $bnStore->getData('address_2'));
        $dbControl->setValue(5, $bnStore->getData('tel'));
        $dbControl->setValue(6, $bnStore->getData('fax'));
        foreach ($aryItem as $key => $value) $dbControl->setValue($key + 7, $value);
        $dbControl->execute();

        $cntResult = $dbControl->getRowCount();
        $dbControl->db_close();

        return $cntResult;
    }

    function getCondition(&$aryItem, $bnStore) {
        // 初期処理
        $strCondition = "";
        $session = new session();

        // 店舗状態
        $strCondition .= "and flug_enable = 1 ";

        // 権限設定
        if (confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) {
            if (confirm::checkInput($bnStore->getData('id'))) {
                $strCondition .= "and t_store.id = ? ";
                array_push($aryItem, $bnStore->getData('id'));
            }

        } else {
            $strCondition .= "and t_store.id = ? ";
            array_push($aryItem, $session->getData('id_store'));
        }

        $strCondition = substr($strCondition, mb_strlen("and "));
        return ($strCondition != "") ? "where " . $strCondition : "";
    }
}
?>

<?php
require_once("web.php");
require_once(DIR_CLASS . "common/confirmBase.php");
require_once(DIR_CLASS . "dao/master/masterDAO.php");
require_once(DIR_CLASS . "dao/charge/chargeDAO.php");
require_once(DIR_CLASS . "dao/receipt/receiptDAO.php");

class confirm extends confirmBase {

    function checkFuncs($funcs, $params, $item, $name) {
        $message = "";
        foreach ( $funcs as $num => $func ) {
            $message = confirm::$func($params[$num], $item, $name);
            if (confirm::checkInput($message)) return $message;
        }
    }

    function checksInput($param, $item, $name) {
        if (!confirm::checkInput($item)) {
            return "{$name}を設定して下さい。\r\n";
        }
    }

    function checksNgWord($param, $item, $name) {
        if (!confirm::checkNgWord($item)) {
            return "{$name}に使用禁止文字が使用されています。\r\n";
        }
    }

    function checksMbLenWidth($param, $item, $name) {
        if (!confirm::checkMbLength($item, $param[0], $param[1])) {
            return "{$name}は{$param[0]}文字以上、{$param[1]}文字以内で設定して下さい。\r\n";
        }
    }

    function checksMbLenMax($param, $item, $name) {
        if (!confirm::checkMbLenMax($item, $param[0])) {
            return "{$name}は{$param[0]}文字以内で設定して下さい。\r\n";
        }
    }

    function checksLineMax($param, $item, $name) {
        if (!confirm::checkLineMax($item, $param)) {
            return "{$name}は{$param}行以内で設定して下さい。\r\n";
        }
    }

    function checksSuuji($param, $item, $name) {
        if (!confirm::checkSuuji($item)) {
            return "{$name}は半角数字で設定して下さい。\r\n";
        }
    }

    function checksSuuji2($param, $item, $name) {
        if (!confirm::checkSuuji2($item)) {
            return "{$name}は半角数字で設定して下さい。\r\n";
        }
    }

    function checksSuuji3($param, $item, $name) {
        if (!confirm::checkSuuji3($item)) {
            return "{$name}は半角数字で設定して下さい。\r\n";
        }
    }

    function checksEijiSuuji($param, $item, $name) {
        if (!confirm::checkEijiSuuji($item)) {
            return "{$name}は半角英数字で設定して下さい。\r\n";
        }
    }

    function checksDate($param, $item, $name) {
       if (!confirm::checkDate($item)) {
            return "{$name}は日付で設定して下さい。[YYYY-MM-DD]\r\n";
        }
    }

    function checksDate2($param, $item, $name) {
       if (!confirm::checkDate2($item)) {
            return "{$name}は年月で設定して下さい。[YYYY-MM]\r\n";
        }
    }

    function checksTime($param, $item, $name) {
        if (!confirm::checkTime($item)) {
            return "{$name}は時間で設定して下さい。\r\n";
        }
    }

    function checksFlug($param, $item, $name) {
        if (!confirm::checkflug($item)) {
            return "{$name}はフラグで設定して下さい。\r\n";
        }
    }

    function checksHiragana($param, $item, $name) {
        if (!confirm::checkHiragana($item)) {
            return "{$name}はひらがなで設定して下さい。\r\n";
        }
    }

    function checksZipCode($param, $item, $name) {
        if (!confirm::checkZipCode($item)) {
            return "{$name}を正しく設定して下さい。\r\n";
        }
    }

    function checksTel($param, $item, $name) {
        if (!confirm::checkTel($item)) {
            return "{$name}を正しく設定して下さい。\r\n";
        }
    }

    function checksMail($param, $item, $name) {
        if (!confirm::checkMail($item)) {
            return "{$name}を正しく設定して下さい。\r\n";
        }
    }

    function checksLonLat($param, $item, $name) {
        if (!confirm::checkLonLat($item)) {
            return "{$name}は整数3桁以内、小数6桁以内で設定して下さい。\r\n";
        }
    }

    function checksMaster($param, $item, $name) {
        $daoMaster = new masterDAO();
        if (!confirm::checkInputArray($daoMaster->master, $item)) {
            return "{$name}のマスタ{$item}が見つかりませんでした。\r\n";
        }
    }

    function checksMasterData($param, $item, $name) {
        $daoMaster = new masterDAO();
        if (!$daoMaster->checkMaster($param[0], $item)) {
            return "{$name}の[{$item}]が{$param[0]}に見つかりませんでした。\r\n";
        }
    }

    function checksMasterData2($param, $item, $name) {
        $daoMaster = new masterDAO();
        if ($daoMaster->checkMaster($param[0], $item, $param[1])) {
            return "{$name}の[{$item}]は設定されています。\r\n";
        }
    }

    function checksAcount($param, $item, $name) {
        $daoCharge = new chargeDAO();
        if ($daoCharge->chkAcount($param[0], $item)) {
            return "{$name}が存在します。\r\n";
        }
    }

    function checksPrint($param, $item, $name) {
        $daoReceipt = new receiptDAO();
        if ($daoReceipt->chkPrint($item)) {
            return "{$name}は完了しています。\r\n";
        }
    }

    function checksDelete($param, $item, $name) {
        $daoReceipt = new receiptDAO();
        if ($daoReceipt->chkDelete($item)) {
            return "{$name}は完了しています。\r\n";
        }
    }
}
?>

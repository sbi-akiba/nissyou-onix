<?php
require_once("web.php");

class session {

    function session($name = "SESID") {
        if (!session_id()) {
            session_name($name);
            session_cache_limiter('nocache');
            session_start();

//            session_regenerate_id();
/*
            $tmp = $_SESSION;
            $_SESSION = array();
            session_destroy();
            session_id(md5(uniqid(rand(), 1)));
            session_start();
            $_SESSION = $tmp;
*/

/*
            if (mt_rand(1, 30)==1) {
                $span = 5 * 60; //
                $sess_file = 'sess_'.session_id();
                $sess_dir_path = ini_get('session.save_path');
                $sess_file_path = $sess_dir_path. '/'. $sess_file;
                $timestamp = filemtime($sess_file_path);

                if (($timestamp + $span) < time()) {
                    $iPHPVer = (int)sprintf('%.3s', str_replace('.', '', PHP_VERSION));
                    if ($iPHPVer>=510) {
                        session_regenerate_id(true);
                    } else {
                        $sess_tmp = $_SESSION;
                        session_destroy();
                        session_id(createUniqueKey(25, true));
                        session_start();
                        $_SESSION = $sess_tmp;
                    }
                }
            }
*/
        }
    }

    function close() {
        session_unset();
        session_destroy();
    }

    function setDataAll($data) {
        foreach ($data as $key => $value){
            $_SESSION[$key] = $value;
        }
    }

    function getDataAll() {
        return $_SESSION;
    }

    function setData($name, $data) {
        $_SESSION[$name] = $data;
    }

    function getData($name) {
        if (!array_key_exists($name, $_SESSION)) {
            return "";
        }
        return $_SESSION[$name];
    }

    function delData($name) {
        if (array_key_exists($name, $_SESSION)) {
        unset($_SESSION[$name]);
        }
    }
}
?>

<?php
require_once("web.php");

class responseJson {

    private $options;
    private $json;
    private $aryData;

    function __construct() {
        $ini_sys = parse_ini_file(DIR_CONFIG . "system.ini");
        $this->options = (isset($ini_sys['JSON_OPTIONS'])) ? $ini_sys['JSON_OPTIONS'] : 0;
    }

    function setJson($_json) {
        $this->json = $_json;
    }

    function getJson() {
        return $this->json;
    }

    function setArray($_array) {
        $this->json = json_encode($_array, $this->options);
    }

    function getArray() {
        return json_decode($this->json, true);
    }

    function send() {
        header("Content-Type: application/json; charset=utf-8");
        echo  $this->getJson();
    }
}
?>

<?php
class html {

    // conrvetへ移動予定
    function text($hashData, $default) {
        $result = "";
        if( is_array($hashData) ) {
            while(list($index, $data) = each($hashData)){
                $result .= ((!strcmp($default, $data['id'])) ? $data['value'] : "");
            }
        }
        return $result;
    }

    // conrvetへ移動予定
    function match($data, $match, $output) {
        return (!strcmp($data, $match)) ? $output : "";
    }

    // conrvetへ移動予定
    function match2($data, $match, $output) {
        return (!strcmp($data, $match)) ? $output : $data;
    }

    // conrvetへ移動予定
    function match3($data, $match, $output1 = "", $output2 = "") {
        return (!strcmp($data, $match)) ? $output1 : $output2;
    }

    // conrvetへ移動予定
    function match4($data, $match, $output) {
        return (strcmp($data, $match)) ? $output : "";
    }

    // ラジオボタン用
    function radio($name, $hashData, $default = null, $key = "id", $value = "value") {
        $result = "";
        if( is_array($hashData) ) {
            while(list($index, $data) = each($hashData)){
                $result .= '<input type="radio" name="' . $name . '" value="' . htmlspecialchars($data[$key]) . '" id="' . $name . $data[$key] . '"' . 
                ((!strcmp($default, $data[$key])) ? " checked=\"checked\"" : "") . '>' . 
                '<label for="' . $name . $data[$key] . '">' . htmlspecialchars($data[$value]) . '</label>　';
            }
        }
        return $result;
    }

    // プルダウン用
    function pulldown($name, $hashData, $default = null, $option = "", $key = "id", $value = "value") {
        $result = "<select name=\"$name\" $option>\n";
        if( is_array($hashData) ) {
            while(list($index, $data) = each($hashData)){
                $result .= ' <option value="' . htmlspecialchars($data[$key]) . '"' . 
                ((!strcmp($default, $data[$key])) ? " selected" : "") . '>' . 
                '<label for="' . $name . $data[$key] . '">' . htmlspecialchars($data[$value]) . '</label>';
            }
        }
        $result .= "</select>\n";
        return $result;
    }

    // チェックボックス用
    function checkbox($name, $hashData, $default = null, $key = "id", $value = "value") {
        $result = "";
        if( is_array($hashData) ) {
            while(list($index, $data) = each($hashData)){
                $result .= '<input type="checkbox" name="' . $name . '[]" value="' . htmlspecialchars($data[$key]) . '" id="' . $name . $data[$key] . '"';

                if( is_array($default) ) {
                    reset($default);
                    while(list($key, $val) = each($default)) {
                        $result .= ((!strcmp($val, $data[$key])) ? " checked=\"checked\"" : "");
                    }
                }

                $result .= '><label for="' . $name . $data[$key] . '">' . htmlspecialchars($data[$value]) . '</label>　';
            }
        }
        return $result;
    }

    // 開始位置
    function pageStart($nowPage, $totalPage, $view) {
        $pointer = ceil($view / 2);
        $front = $pointer - 1;
        $back = $view - $pointer;
        $startPage = 0;

        if ($totalPage < $view) {
            $startPage = 1;

        } else if ($nowPage - $front > 1) {
            if ($nowPage + $back > $totalPage) {
                $startPage = $nowPage - $front - $back + ($totalPage - $nowPage);
            } else {
                $startPage = $nowPage - $front;
            }
        } else {
            $startPage = 1;
        }

        return $startPage;
    }

    // 終了位置
    function pageEnd($nowPage, $totalPage, $view) {
        $pointer = ceil($view / 2);
        $front = $pointer - 1;
        $back = $view - $pointer;
        $endPage = 0;

        if ($totalPage < $view) {
            $endPage = $totalPage;

        } else if ($nowPage + $back < $totalPage) {
            if ($nowPage - $front < 1) {
                $endPage = $nowPage + $back + $front - ($nowPage - 1);
            } else {
                $endPage = $nowPage + $back;
            }
        } else {
            $endPage = $totalPage;
        }

        return $endPage;
    }

    function prev_key($array,$key) {
        $tmp=false;
        reset($array);
        while (list($index, $data) = each($array)) {
            if($data['id']==$key) {
                return $tmp;
            }
            $tmp=$data['id'];
        }
        return false;
    }

    function next_key($array,$key) {
        $tmp=false;
        reset($array);
        while (list($index, $data) = each($array)) {
            if($data['id']==$key) {
                $tmp=$data['id'];
            } elseif($tmp!==false) {
                return $data['id'];
            }
        }
        return false;
    }
}
?>

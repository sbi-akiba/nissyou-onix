<?php
require_once("web.php");
require_once(DIR_CLASS . "common/dbBase.php");
require_once(DIR_CLASS . "common/convert.php");

class db extends dbBase {

//-------------------------------------------------

    var $DateFormat = "";

    // 初期処理
    function __construct($iniFileName = "db.ini") {

        $ini_db = parse_ini_file(DIR_CONFIG . $iniFileName);
        $dbHost = $ini_db['DB_HOST'];
        $dbName = $ini_db['DB_NAME'];
        $dbUser = $ini_db['DB_USER'];
        $dbPass = $ini_db['DB_PASS'];
        $dbPort = $ini_db['DB_PORT'];

//        $host = "pgsql:dbname={$dbName} host={$dbHost} $dbOptions port={$dbPort}";
        $host = "mysql:dbname={$dbName};host={$dbHost}";
        parent::setHost($host);
        parent::setUser($dbUser);
        parent::setPass($dbPass);

        // オプション部分の作り込みは出来ていないので直設定
        parent::setOption(array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4'));
    }

    // 接続処理
    function db_connect() {
        return parent::connect();
    }

    // 切断処理
    function db_close() {
        $this->close();
    }

    function fetch() {
        try {
            return $this->getFetch();
//            return convert::convDateFormat($this->getFetch());
        } catch (PDOException $e){
            throw $e;
        }
    }

    function fetchAll() {
        try {
            return $this->getFetchAll();
//            return convert::convDateFormat($this->getFetchAll());
        } catch (PDOException $e){
            throw $e;
        }
    }
}
?>

<?php
require_once("web.php");

class convert {

    /* XSSエスケープ */
    function escapeXSS($prmItem) {
        if (is_array($prmItem)) {
            return array_map(array("convert", "escapeXSS"), $prmItem);
        } else {
            return htmlentities($prmItem, ENT_QUOTES, 'UTF-8');
        }
    }

    /* XSSアンエスケープ */
    function unescapeXSS($prmItem) {
        if (is_array($prmItem)) {
            return array_map(array("convert", "unescapeXSS"), $prmItem);
        } else {
            if (phpversion() >= "4.3.0") {
                return html_entity_decode($prmItem, ENT_QUOTES, 'UTF-8');
            } else {
                $trans_tbl = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
                $trans_tbl = array_flip ($trans_tbl);
                foreach( $trans_tbl as $key => $value ) {
                    $prmItem = ereg_replace($key, $value, $prmItem);
                }
                return $prmItem;
            }
        }
    }

    /* HTTP/HTTPS判定 */
    function getProtocol() {
        if ((array_key_exists('HTTPS', $_SERVER)) && (($_SERVER['HTTPS']=='on'))) {
            return "https";
        } else if ((array_key_exists('HTTP_REFERER', $_SERVER)) && (strpos($_SERVER['HTTP_REFERER'], 'https://', 0) === 0)) {
            return "https";
        } else {
            return "http";
        }
    }

    /* 日付変換 */
    function convDateFormat($prmItem) {
        if (is_array($prmItem)) {
            return array_map(array("convert", "convdateFormat"), $prmItem);
        } else {
            $prmItem = (preg_match("/^\d{4}\-\d{2}\-\d{2}$/", $prmItem)) ? convert::dateFormat($prmItem) : $prmItem;
            $prmItem = (preg_match("/^\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}$/", $prmItem)) ? convert::dateTimeFormat($prmItem) : $prmItem;
            return $prmItem;
        }
    }

    /* 改行エスケープ */
    function escapeLine($prmItem){
        $prmItem = mb_ereg_replace("<BR />","\r\n",$prmItem);
        $prmItem = mb_ereg_replace("<br />","\r\n",$prmItem);
        $prmItem = mb_ereg_replace("<BR>","\r\n",$prmItem);
        return mb_ereg_replace("<br>","\r\n",$prmItem);
    }

    /* 改行アンエスケープ */
    function unescapeLine($prmItem){
        $prmItem = mb_ereg_replace("\r\n","\r",$prmItem);
        $prmItem = mb_ereg_replace("\n","\r",$prmItem);
        return mb_ereg_replace("\r","<br />",$prmItem);
    }

    /* 改行アンエスケープ2 */
    function unescapeLine2($prmItem){
        $prmItem = mb_ereg_replace("\r\n","\r",$prmItem);
        $prmItem = mb_ereg_replace("\n","\r",$prmItem);
        return mb_ereg_replace("\r"," ",$prmItem);
    }

    /* 最上位コメント取得 */
    function getTopComment($prmItem) {
        $prmItem = mb_ereg_replace("\r\n","\r",$prmItem);
        $prmItem = mb_ereg_replace("\n","\r",$prmItem);
        $aryTmp = explode("\r", $prmItem);
        if (count($aryTmp) > 0) $prmItem = $aryTmp[0];
        return $prmItem;
    }

    /* コメント短縮 */
/*
    function commentCut($prmItem, $start, $end, $format = "") {
        $strNum = strlen(mb_convert_encoding($prmItem, 'SJIS', 'UTF-8'));
        return (($end - $start) <= $strNum) ? mb_strimwidth($prmItem, $start, $end, $format, "UTF-8") : $prmItem;
    }
*/

    function commentCut($prmItem, $start, $end, $format = "") {
        $strNum = mb_strlen($prmItem);
        return (($end - $start) <= $strNum) ? mb_substr($prmItem, $start, $end) . $format : mb_substr($prmItem, $start, $end);
    }

    /* 価格フォーマット設定 */
    function priceFormat($prmItem) {
        return (is_numeric($prmItem)) ? number_format($prmItem) : $prmItem;
    }
/*
    function priceFormat($prmItem) {
        if (preg_match("/\./", $prmItem)) {
            list($price1, $price2) = split("\.", $prmItem);
            if ($price2 > 0) {
                return number_format($prmItem, 1);
            } else {
                return number_format($price1);
            }
        }
        return number_format($prmItem);
    }
*/

    /* 価格フォーマット解除 */
    function priceNotFormat($prmItem) {
        if (preg_match("/\./", $prmItem)) {
            list($price1, $price2) = split("\.", $prmItem);
            if ($price2 > 0) {
                return $prmItem;
            } else {
                return $price1;
            }
        }
        return $prmItem;
    }

    function delim($prmItem, $delim, $prmNum = 0) {
        if (preg_match("/" . $delim . "/", $prmItem)) {
            $aryItem = split("\.", $prmItem);
            if ($prmNum != 0) {
                return (array_key_exists($prmNum - 1, $aryItem)) ? $aryItem[$prmNum - 1] : "";
            } else {
                return $aryItem;
            }
        }
        return $prmItem;
    }

   // 日付フォーマット
    function dateFormat($prmItem) {
        return (strtotime($prmItem)) ? date("Y年m月d日", strtotime($prmItem)) : $prmItem;
    }

   // 日付フォーマット2
    function dateFormat2($prmItem) {
        return (strtotime($prmItem)) ? date("Y年m月", strtotime($prmItem . "-01")) : $prmItem;
    }

   // 日付フォーマット3
    function dateFormat3($prmItem) {
        return (strtotime($prmItem)) ? date("Y/m/d", strtotime($prmItem)) : $prmItem;
    }

   // 日付フォーマット4
    function dateFormat4($prmItem) {
        return (strtotime($prmItem)) ? date("Y-m-d", strtotime($prmItem)) : $prmItem;
    }

    // 日付時間フォーマット
    function dateTimeFormat($prmItem) {
        return (strtotime($prmItem)) ? date("Y年m月d日 H時i分s秒", strtotime($prmItem)) : $prmItem;
    }

    // 坪数フォーマット
    function areaFormat($prmItem) {
        return ($prmItem == "0.00") ? "" : $prmItem;
    }

    function trim($str, $chars="\s｡｡") {
        $str = mb_ereg_replace("^[$chars]+", "", $str);
        $str = mb_ereg_replace("[$chars]+$", "", $str);
        return $str;
    }

    // 全文置換
    function replace($prmItem, $prmArrays) {
        foreach( $prmArrays as $key => $value ) {
            if( !is_array($value) ) {
//                $value = convert::unescapeXSS($value);
                $prmItem = str_replace("_%" . $key . "%_", $value, $prmItem);
            }
        }
        return $prmItem;
    }

    /**
     * 前方一致
     * $haystackが$needleから始まるか判定します。
     * @param string $haystack
     * @param string $needle
     * @return boolean
     */
    function startsWith($haystack, $needle){
        return strpos($haystack, $needle, 0) === 0;
    }
     
    /**
     * 後方一致
     * $haystackが$needleで終わるか判定します。
     * @param string $haystack
     * @param string $needle
     * @return boolean
     */
    function endsWith($haystack, $needle){
        $length = (strlen($haystack) - strlen($needle));
        // 文字列長が足りていない場合はFALSEを返します。
        if($length < 0) return FALSE;
        return strpos($haystack, $needle, $length) !== FALSE;
    }
     
    /**
     * 部分一致
     * $haystackの中に$needleが含まれているか判定します。
     * @param string $haystack
     * @param string $needle
     * @return boolean
     */
    function matchesIn($haystack, $needle){
        return strpos($haystack, $needle) !== FALSE;
    }

    // 暗号化
    function encrypt($prmItem) {
        return md5($prmItem);
    }

    // パディング
    // item パディング対象
    // pad パディング使用文字
    // plase 桁数
    // 例）padding("1", "0", 4) = "0001"
    function padding($item, $pad, $place) {
        $padding_num = $place - strlen($item);
        return str_repeat($pad, $padding_num) . $item;
    }

    // パディング2
    // item パディング対象
    // pad パディング使用文字
    // plase 桁数
    // 例）padding("1", "0", 4) = "1000"
    function padding2($item, $pad, $place) {
        $padding_num = $place - strlen($item);
        return $item . str_repeat($pad, $padding_num);
    }

    // ID暗号化
    // IDを数字による暗号化を行う
    // 日付とIDを混ぜているだけ
    // 分-秒-ID（4ケタ）を分解（4,2,5,7,1,6,3,0）
    function encryptId($strId, $strDate) {
        $strDate = date("YmdHis", strtotime($strDate));
        $strNo = strval(substr($strDate, 10, 4)) . strval(convert::padding($strId, "0", 4));
        return substr($strNo, 4, 1) . substr($strNo, 2, 1) . substr($strNo, 5, 1) . substr($strNo, 7, 1) . substr($strNo, 1, 1) . substr($strNo, 6, 1) . substr($strNo, 3, 1) . substr($strNo, 0, 1);
    }

    // ID復号化
    // encryptIdで暗号化したIDを復号（4ケタ）
    // 配列でIDと分秒を返却
    function decryptId($strNo) {
        $result = array("", "");
        if (confirm::checkSuuji(substr($strNo, 0, 1) . substr($strNo, 2, 1) . substr($strNo, 5, 1) . substr($strNo, 3, 1))) {
            $result[0] = intval(substr($strNo, 0, 1) . substr($strNo, 2, 1) . substr($strNo, 5, 1) . substr($strNo, 3, 1));
        }

        if (confirm::checkSuuji(substr($strNo, 7, 1) . substr($strNo, 4, 1) . substr($strNo, 1, 1) . substr($strNo, 6, 1))) {
            $result[1] = substr($strNo, 7, 1) . substr($strNo, 4, 1) . ":" . substr($strNo, 1, 1) . substr($strNo, 6, 1);
        }

        return $result;
    }

    // ID暗号化
    // IDを数字による暗号化を行う
    // 日付とIDを混ぜているだけ
    // 分-秒-ID（6ケタ）を分解（4,9,2,5,7,1,8,6,3,0）
    function encryptId2($strId, $strDate) {
        $strDate = date("YmdHis", strtotime($strDate));
        $strNo = strval(substr($strDate, 10, 4)) . strval(convert::padding($strId, "0", 6));
        return substr($strNo, 4, 1) . substr($strNo, 9, 1) . substr($strNo, 2, 1) . substr($strNo, 5, 1) . substr($strNo, 7, 1) . substr($strNo, 1, 1) . substr($strNo, 8, 1) . substr($strNo, 6, 1) . substr($strNo, 3, 1) . substr($strNo, 0, 1);
    }

    // ID復号化
    // encryptIdで暗号化したIDを復号（6ケタ）
    // 配列でIDと分秒を返却
    function decryptId2($strNo) {
        $result = array("", "");
        if (confirm::checkSuuji(substr($strNo, 0, 1) . substr($strNo, 3, 1) . substr($strNo, 7, 1) . substr($strNo, 4, 1) . substr($strNo, 6, 1) . substr($strNo, 1, 1))) {
            $result[0] = intval(substr($strNo, 0, 1) . substr($strNo, 3, 1) . substr($strNo, 7, 1) . substr($strNo, 4, 1) . substr($strNo, 6, 1) . substr($strNo, 1, 1));
        }

        if (confirm::checkSuuji(substr($strNo, 9, 1) . substr($strNo, 5, 1) . substr($strNo, 2, 1) . substr($strNo, 8, 1))) {
            $result[1] = substr($strNo, 9, 1) . substr($strNo, 5, 1) . ":" . substr($strNo, 2, 1) . substr($strNo, 8, 1);
        }

        return $result;
    }

    // ID暗号化
    // IDを数字による暗号化を行う
    // 日付とIDを混ぜているだけ
    // 分-秒-ID（7ケタ）を分解（4,9,2,5,7,1,8,10,6,3,0）
    function encryptId3($strId, $strDate) {
        $strDate = date("YmdHis", strtotime($strDate));
        $strNo = strval(substr($strDate, 10, 4)) . strval(convert::padding($strId, "0", 7));
        return substr($strNo, 4, 1) . substr($strNo, 9, 1) . substr($strNo, 2, 1) . substr($strNo, 5, 1) . substr($strNo, 7, 1) . substr($strNo, 1, 1) . substr($strNo, 8, 1) . substr($strNo, 10, 1) . substr($strNo, 6, 1) . substr($strNo, 3, 1) . substr($strNo, 0, 1);
    }

    // ID復号化
    // encryptIdで暗号化したIDを復号（7ケタ）
    // 配列でIDと分秒を返却
    function decryptId3($strNo) {
        $result = array("", "");
        if (confirm::checkSuuji(substr($strNo, 0, 1) . substr($strNo, 3, 1) . substr($strNo, 8, 1) . substr($strNo, 4, 1) . substr($strNo, 6, 1) . substr($strNo, 1, 1) . substr($strNo, 7, 1))) {
            $result[0] = intval(substr($strNo, 0, 1) . substr($strNo, 3, 1) . substr($strNo, 8, 1) . substr($strNo, 4, 1) . substr($strNo, 6, 1) . substr($strNo, 1, 1) . substr($strNo, 7, 1));
        }

        if (confirm::checkSuuji(substr($strNo, 10, 1) . substr($strNo, 5, 1) . substr($strNo, 2, 1) . substr($strNo, 9, 1))) {
            $result[1] = substr($strNo, 10, 1) . substr($strNo, 5, 1) . ":" . substr($strNo, 2, 1) . substr($strNo, 9, 1);
        }

        return $result;
    }


    static public function dataEncrypt2($value) {
        //Base64でエンコードする  
        $encodedValue = base64_encode($value);

        /* 暗号モジュールをオープンします */
        $td = self::getCryptModule();

        /* データを暗号化します */
        $encrypted = mcrypt_generic($td, $encodedValue);

        //暗号化された文字をBase64でエンコードする  
        $encodedEncryptValue = base64_encode($encrypted);

        /* 暗号化ハンドラを終了します */
        mcrypt_generic_deinit($td);

        /* モジュールを閉じます */
        mcrypt_module_close($td);

        return $encodedEncryptValue;
    }


    static public function dataDecrypt2($value){
        //Base64デコードする
        $decodedValue = base64_decode($value);

        /* 暗号モジュールをオープンします */
        $td = self::getCryptModule();

        /* 暗号化された文字列を復号します */
        $decrypted = mdecrypt_generic($td, $decodedValue);

        /* 復号ハンドルを終了し、モジュールを閉じます */
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);

        //Base64デコードする
        $decodedDecryptValue = base64_decode($decrypted);

        return $decodedDecryptValue;
    }

    static private function getCryptModule() {
     /* 暗号モジュールをオープンします */
     //$td = mcrypt_module_open('tripledes', '', MCRYPT_MODE_CBC, '');
     $td = mcrypt_module_open(MCRYPT_RIJNDAEL_256, '', MCRYPT_MODE_ECB, '');

     /* IV を作成し、キー長を定義します。Windows では、かわりに MCRYPT_RAND を使用します */  
     $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
     $ks = mcrypt_enc_get_key_size($td);

     /* キーを作成します */
     $key = substr(md5('very secret key'), 0, $ks);

     /* 復号用の暗号モジュールを初期化します */
     mcrypt_generic_init($td, $key, $iv);

     return $td;
    }

    // 暗号化
    function dataEncrypt($prmKey, $prmItem) {
        try {
            $base64Item = base64_encode($prmItem);
            $resource = mcrypt_module_open(MCRYPT_RIJNDAEL_256,'',MCRYPT_MODE_CFB,'');
            $prmKey = substr(md5($prmKey), 0, mcrypt_enc_get_key_size($resource));
            $mcryptIv = substr(md5($prmKey), 0, mcrypt_enc_get_iv_size($resource));

            mcrypt_generic_init($resource, $prmKey, $mcryptIv);
            $encryptItem = mcrypt_generic($resource, $base64Item);
            mcrypt_generic_deinit($resource);
            mcrypt_module_close($resource);
            return base64_encode($encryptItem);

        } catch (Exception $e) {
            return "";
        }
    }

    // 復号化
    function dataDecrypt($prmKey, $prmItem) {
        try {
            $base64Item = base64_decode($prmItem);
            $resource = mcrypt_module_open(MCRYPT_RIJNDAEL_256,'',MCRYPT_MODE_CFB,'');
            $prmKey = substr(md5($prmKey), 0, mcrypt_enc_get_key_size($resource));
            $mcryptIv = substr(md5($prmKey), 0, mcrypt_enc_get_iv_size($resource));

            mcrypt_generic_init($resource, $prmKey, $mcryptIv);
            $decryptItem = mdecrypt_generic($resource, $base64Item);
            mcrypt_generic_deinit($resource);
            mcrypt_module_close($resource);
            return base64_decode($decryptItem);

        } catch (Exception $e) {
            return "";
        }
    }

    // 指定文字数にて改行
    function mb_wordwrap( $str, $width=35, $break=PHP_EOL ) {
        $c = mb_strlen($str);
        $arr = array();
        for ($i=0; $i<=$c; $i+=$width) {
            $arr[] = mb_substr($str, $i, $width);
        }
        return implode($break, $arr);
    }

    function getAddressToGps($prmAddress = "") {
        $res = array();
        $req = 'http://maps.googleapis.com/maps/api/geocode/xml';
        $req .= '?address='.urlencode($prmAddress);
        $xml = simplexml_load_file($req) or die('XML parsing error');

        if ($xml->status == 'OK') {
            $location = $xml->result->geometry->location;
            $res['lat'] = (string)number_format((float)$location->lat[0], 6);
            $res['lng'] = (string)number_format((float)$location->lng[0], 6);
        } else {
            error_log(date("[Y/m/d H:i:s] ") . print_r($xml,true), "3", DIR_DATA . "gps_error.log");
            $res['lat'] = "";
            $res['lng'] = "";
        }
        return $res;
    }

    function getAddressToGps2($prmAddress = "") {
        $res = array();
        $req = 'http://www.geocoding.jp/api/';
        $req .= '?q='.urlencode($prmAddress);
        $xml = simplexml_load_file($req) or die('XML parsing error');

        if ($xml->error != '001') {
            $coordinate = $xml->coordinate;
            $res['lat'] = (string)number_format((float)$coordinate->lat[0], 6);
            $res['lng'] = (string)number_format((float)$coordinate->lng[0], 6);
        } else {
            error_log(date("[Y/m/d H:i:s] ") . "GPS2: " . print_r($xml,true), "3", DIR_DATA . "gps_error2.log");
            $res['lat'] = "";
            $res['lng'] = "";
        }
        return $res;
    }
}
?>

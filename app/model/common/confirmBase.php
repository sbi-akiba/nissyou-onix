<?php

class confirmBase {

    function checkInput($Item) {
      return (!is_null($Item) && isset($Item) &&!confirm::checkComp($Item, "")) ? true : false;
    }

    function checkInputArray($item, $key) {
        return (array_key_exists($key, $item)) ? confirm::checkInput($item[$key]) : false;
    }

    function checkArray($item) {
        return ((is_array($item) && isset($item))) ? true : false;
    }

    function checkComp($Item1, $Item2) {
        return (!strcmp($Item1, $Item2)) ? true : false;
    }

    function checkLength($str,$len1,$len2) {
        return (strlen($str) >= $len1 && strlen($str) <= $len2) ? true : false;
    }

    function checkMbLength($str,$len1,$len2) {
        return (mb_strlen($str, 'UTF-8') >= $len1 && mb_strlen($str, 'UTF-8') <= $len2) ? true : false;
    }

    function checkMbLenMax($str,$len) {
        return (mb_strlen($str, 'UTF-8') <= $len) ? true : false;
    }

    function checkLineMax($str,$len) {
        $aryTemp = explode("\n", str_replace(array("\r\n", "\r", "\n"), "\n", $str));
        return (count($aryTemp) <= $len) ? true : false;
    }

    function checkSuuji($str) {
        return (preg_match("/^[0-9]+$/", $str)) ? true : false;
    }

    function checkSuuji2($str) {
        return (preg_match("/^[0-9.]+$/", $str)) ? true : false;
    }

    function checkSuuji3($str) {
        return (preg_match("/^[0-9-]+$/", $str)) ? true : false;
    }

    function checkEiji($str) {
        return (preg_match("/^[A-Za-z]+$/", $str)) ? true : false;
    }

    function checkEijiSuuji($str) {
        return (preg_match("/^[A-Za-z0-9]+$/", $str)) ? true : false;
    }

    function checkEijiSuuji2($str) {
        return (preg_match("/^[A-Za-z0-9-]+$/", $str)) ? true : false;
    }

    function checkEijiSuuji3($str) {
        return (preg_match("/^[A-Za-z0-9-_]+$/", $str)) ? true : false;
    }

    function checkPercent($str) {
        return ($str <= 100) ? true : false;
    }

    function checkMail($str){
        if(preg_match('/^[-+.\\w]+@[-a-z0-9]+(\\.[-a-z0-9]+)*\\.[a-z]{2,6}$/i', $str)){
            $aryStr = explode('@',$str);
            if (checkdnsrr(end($aryStr),'MX')) return true;
        }
        return false;

        //if (preg_match('/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/', $str)) {
        //return (preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $str)) ? true : false;
        //return (preg_match('/\A(?:(?:(?:(?:[a-zA-Z0-9_!#\$\%&\'*+\/=?\^`{}~|\-]+)(?:\.(?:[a-zA-Z0-9_!#\$\%&\'*+\/=?\^`{}~|\-]+))*)|(?:"(?:\\\\[^\r\n]|[^\\\\"])*")))\@(?:(?:(?:(?:[a-zA-Z0-9_!#\$\%&\'*+\/=?\^`{}~|\-]+)(?:\.(?:[a-zA-Z0-9_!#\$\%&\'*+\/=?\^`{}~|\-]+))*)|(?:\[[\x21-\x5a\x5e-\x7e]*\])))\z/', $str)) ? true : false;
        //return (preg_match('/^(?:[^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff])|"[^\\\\\x80-\xff\n\015"]*(?:\\\\[^\x80-\xff][^\\\\\x80-\xff\n\015"]*)*")(?:\.(?:[^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff])|"[^\\\\\x80-\xff\n\015"]*(?:\\\\[^\x80-\xff][^\\\\\x80-\xff\n\015"]*)*"))*@(?:[^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff])|\[(?:[^\\\\\x80-\xff\n\015\[\]]|\\\\[^\x80-\xff])*\])(?:\.(?:[^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff])|\[(?:[^\\\\\x80-\xff\n\015\[\]]|\\\\[^\x80-\xff])*\]))*$/', $str)) ? true : false;
    }

    function checkUrl($str){
        return (preg_match('/^(https?)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+)$/', $str)) ? true : false;
    }

    function checkUrl2($str){
        return (preg_match("/^[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+$/", $str)) ? true : false;
    }

    function checkHiragana($str){
        return (mbereg('^([ぁ-ん]|[ー]|[ ]|[　]){1,}$',$str)) ? true : false;
    }

    function checkKatakana($str){
        return (mbereg('^([ァ-ン]|[ー]|[ ]|[　]){1,}$',$str)) ? true : false;
    }

    function checkNgWord($str) {
        $ng_words = array(
            "\"","\\","\t","\r","\n",
            "─","│","┌","┐","┘","└","├","┬","┤","┴","┼","━","┃","┏","┓",
            "┛","┗","┣","┳","┫","┻","╋","┠","┯","┨","┷","┿","┝","┰","┥","┸",
            "╂",
            "㍉","㌔","㌢","㍍","㌘","㌧","㌃","㌶","㍑","㍗","㌍","㌦","㌣","㌫","㍊","㌻",
            "㎜","㎝","㎞","㎎","㎏","㏄","㎡","㍻",
            "〝","〟","㊤","㊥","㊦","㊧","㊨","㍾","㍽","㍼",
            "≒","≡","∫","∮","∑","√","⊥","∠","∟","⊿","∩","∪");

        foreach($ng_words as $word) {
            if (strrpos($str, $word)) {
                return false;
            }
        }
        return true;
    }

    function checkPrice($str) {
//        return (preg_match("/^\d+\.?\d{1}$|^\d+$|^$/",$str)) ? true : false;
        return (preg_match("/^[0-9]{1,6}.[0-9]{1}$/", $str)) ? true : false;
    }

    function checkColor($str) {
        return (preg_match("/^#{1}\w{6}$/",$str)) ? true : false;
    }

    function checkZipCode($str) {
        return (preg_match("/^\d{3}\-\d{4}$/", $str)) ? true : false;
    }

    function checkTel($str) {
//        return (preg_match("/^[0-9-]+$/", $str)) ? true : false;
        return (preg_match("/^\d{1,4}\-\d{1,4}\-\d{1,4}$/", $str)) ? true : false;
    }

    // 階数チェック
    function checkRank($str) {
        return (preg_match("/^[bB]?[0-9]+$/", $str)) ? true : false;
    }


    // 期間チェック
    function checkDateScope($fromDate, $toDate, $days) {
        $dtFrom = strtotime($fromDate);
        $dtTo = strtotime($toDate);
        $dtInterval = round(($dtTo - $dtFrom) / (60*60*24));
        return ((0 <= $dtInterval) && ($dtInterval <= $days - 1)) ? true : false;
    }

    // 期間指定フォーマットチェック（年・月・日のどれかのみ設定可能）
    function checkDateFormat($str) {
        $count = 0;
        $aryDate = explode("-", $str);

        foreach ($aryDate as $tmpDate) {
            if (intVal($tmpDate) <> 0) $count++;
        }

        return ($count == 1) ? true : false;
    }

    /* フラグチェック */
    function checkFlug($str) {
        return (confirm::checkComp($str, "0") or confirm::checkComp($str, "1")) ? true : false;
    }

    /* 拡張子チェック */
    function checkExtension($strExt, $aryExt) {
        return (in_array($strExt,$aryExt)) ? true : false;
    }

    /* 日付チェック */
    function checkDate($str) {
        if ((preg_match("/^\d{4}\-\d{1,2}\-\d{1,2}$/", $str)) || (preg_match("/^\d{4}\-\d{1,2}\-\d{1,2} \d{2}:\d{2}:\d{2}$/", $str))) {
            $date = explode("-", $str);
//            $date = date_parse($str);

            if ($date) {
                return (checkdate($date[1], $date[2], $date[0])) ? true : false;
            }
        }
        return false;
    }

    /* 日付チェック */
    function checkDate2($str) {
        $str .= "-01";
        return confirm::checkDate($str);
    }

    /* 時間チェック */
    function checkTime($str) {
        if (preg_match("/^\d{2}\d{2}$/", $str)) {
            if (substr($str, 0, 2) < 0 || substr($str, 0, 2) > 23) return false;
            if (substr($str, 2, 2) < 0 || substr($str, 2, 2) > 59) return false;
            return true;
        }
        return false;
    }

    /* 日時チェック */
    function checkDateTime($str) {
        if (preg_match("/^\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}$/", $str)) {
            $date = date_parse($str);
            if ($date && $date['error_count'] == 0) {
                return ((checkdate($date['month'], $date['day'], $date['year'])) && (confirm::checktime($date['hour'], $date['minute'], $date['second']))) ? true : false;
            }
        }
        return false;
    }

    /* 緯度・経度チェック */
    function checkLonLat($str) {
        return (preg_match("/^[-]?([1-9]\d*|0{1,3})(\.\d{1,6})?$/", $str)) ? true : false;
    }

    // 前方一致
    function startsWith($Item1, $Item2) {
        return strpos($Item1, $Item2, 0) === 0;
    }

    // 後方一致
    function endsWith($Item1, $Item2) {
        $length = (strlen($Item1) - strlen($Item2));
        // 文字列長が足りていない場合はFALSEを返却
        if($length < 0) return FALSE;
        return strpos($Item1, $Item2, $length) !== FALSE;
    }

    // 部分一致
    function matchesIn($Item1, $Item2) {
        return strpos($Item1, $Item2) !== FALSE;
    } 
}
?>

<?php
class dbBase {

    var $host = "";
    var $user = "";
    var $pass = "";
    var $option = array();
    var $pdoObject;
    var $pdoStatement;
    var $pdoParam;
    var $errorCode = 0;
    var $errorMessage = "";

//-------------------------------------------------

    function setPdoObject( $_pdoObject ) {
        $this->PdoObject = $_pdoObject;
    }

    function getPdoObject() {
        return $this->pdoObject;
    }

    function setHost( $host ) {
        $this->host = $host;
    }

    function getHost() {
        return $this->host;
    }

    function setUser( $user ) {
        $this->user = $user;
    }

    function getUser() {
        return $this->user;
    }

    function setPass( $pass ) {
        $this->pass = $pass;
    }

    function getPass() {
        return $this->pass;
    }

    function setOption( $option ) {
        $this->option = $option;
    }

    function getOption() {
        return $this->option;
    }

    function setErrorCode( $errorCode ) {
        $this->errorCode = $errorCode;
    }

    function getErrorCode() {
        return $this->errorCode;
    }

    function setErrorMessage( $errorMessage ) {
        $this->errorMessage = $errorMessage;
    }

    function getErrorMessage() {
        return $this->errorMessage;
    }

    function connect() {
        try {
            $this->pdoObject = new PDO ("{$this->getHost()}", $this->getUser(), $this->getPass(), $this->getOption());
//            $this->pdoObject->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_TO_STRING);
//            $this->query("SET NAMES 'UTF8'");
//            $this->query("set client_encoding to UTF8");

        } catch (PDOException $e){
            throw $e;
        }
    }

    // 切断
    function close() {
        try {
            $this->pdoObject = null;

        } catch (PDOException $e){
            throw $e;
        }
    }

    function query($query) {
        try {
            $this->prepare($query);
            $this->execute();

        } catch (PDOException $e){
            throw $e;
        }
    }

    function prepare($query) {
        try {
            $this->pdoStatement = $this->pdoObject->prepare($query);

        } catch (PDOException $e){
            throw $e;
        }
    }

    function execute() {
        try {
            $this->pdoStatement->execute();

        } catch (PDOException $e){
            throw $e;
        }
    }

    function beginTransaction() {
        try {
            $this->pdoObject->beginTransaction();

        } catch (PDOException $e){
            throw $e;
        }
    }

    function commit() {
        try {
            $this->pdoObject->commit();

        } catch (PDOException $e){
            throw $e;
        }
    }

    function rollback() {
        try {
            $this->pdoObject->rollback();

        } catch (PDOException $e){
            throw $e;
        }
    }

    function setValue($id, $value) {
        $this->pdoStatement->bindValue($id, $value);
    }

    function setValueInt($id, $value) {
        $value = ($value ==="") ? null : $value;
        $this->pdoStatement->bindValue($id, $value);
    }

    function setValueDate($id, $value) {
        $value = ($value ==="") ? null : $value;
        $this->pdoStatement->bindValue($id, $value);
    }

    function setValueString($id, $value) {
        $this->pdoStatement->bindValue($id, $value, PDO::PARAM_STR);
    }

    function getFetch() {
        try {
            return $this->pdoStatement->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e){
            throw $e;
        }
    }

    function getFetchAll() {
        try {
            return $this->pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e){
            throw $e;
        }
    }

    function getColumnCount() {
        try {
            return $this->pdoStatement->columnCount();
        } catch (PDOException $e){
            throw $e;
        }
    }

    function getRowCount() {
        try {
            return $this->pdoStatement->rowCount();
        } catch (PDOException $e){
            throw $e;
        }
    }

    function getInsertId() {
        try {
            return $this->pdoStatement->lastInsertId();
        } catch (PDOException $e){
            throw $e;
        }
    }

    // 未定義の関数
    // 直接PDOに渡す
    function __call($method, $argument) {
        if (is_array($argument)) {
            $result =  call_user_func_array(array($this->pdoObject, $method), $argument);
        }
        else {
            $result =  call_user_func(array($this->pdoObject, $method), $argument);
        }

        return $result;
    }

    // 未定義のメンバ変数取得
    // 直接PDOに渡す
    public function __get($name) {
        return $this->pdoObject->{$name};
    }

    // 未定義メンバ変数設定
    // 直接PDOに渡す
    public function __set($name, $value) {
        $this->pdoObject->{$name} = $value;
    }
}
?>

<?php
    require_once("web.php");
    require_once(DIR_CLASS . "common/confirm.php");
    require_once(DIR_CLASS . "common/session.php");
    require_once(DIR_CLASS . "common/responseJson.php");
    require_once(DIR_CLASS . "bean/session/sessionCheckBean.php");

    // セッションチェック
    $filter_session = new session();
    $bnSessionCheck = new sessionCheckBean();
    $message = $bnSessionCheck->chkSession($filter_session);

    // エラーの場合
    if (confirm::checkInput($message)) {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
            $resJson_session = new responseJson();
            $resJson_session->setArray(array('status'=>'2'));
            $resJson_session->send();
            exit;
        } else {
            $filter_session->setData('message', $message);
            require_once(DIR_VIEW . "include/chache.php");
            require_once(DIR_VIEW . "error/transfer.php");
            exit;
        }

    } else {
        // 時間を更新
        $filter_session->setData("time", strtotime(date('Y-m-d H:i:s')));
    }
?>

<?php
    require_once("web.php");
    require_once(DIR_CLASS . "common/confirm.php");
    require_once(DIR_CLASS . "common/responseJson.php");

    if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {
    } else {
        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "error/transfer.php");
        exit;
    }

?>

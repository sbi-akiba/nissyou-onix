<?php
require_once("web.php");
require_once(DIR_CLASS . "common/confirm.php");

class chargeCheckBean {

// ------------------------------------------------

    // ログインパラメータチェック
    function chkLogin($bnCharge) {
        $message = "";

        $message .= confirm::checkFuncs(array("checksInput", "checksMbLenMax"), array("", array(40)), $bnCharge->getData("mail"), "ユーザID");
        $message .= confirm::checkFuncs(array("checksInput", "checksMbLenWidth"), array("", array(4, 40)), $bnCharge->getData("password"), "パスワード");

        return $message;
    }

    // 担当者詳細パラメータチェック
    function chkDetail($bnCharge) {
        $message = "";

        $message .= confirm::checkFuncs(array("checksInput", "checksSuuji"), array("", ""), $bnCharge->getData("id"), "担当者ID");

        return $message;
    }

    // 担当者登録パラメータチェック
    function chkRegist($bnCharge) {
        $session = new session();
        $message = "";

        // 登録権限
        if (!confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) $message .= "登録出来る権限がありません。";

        // 店舗名
        $message .= confirm::checkFuncs(array("checksInput", "checksMasterData"), array("", array("t_store")), $bnCharge->getData("id_store"), "店舗名");

        // 担当者名
        $message .= confirm::checkFuncs(array("checksInput", "checksMbLenMax"), array("", array(20)), $bnCharge->getData("name"), "担当者名");

        // メールアドレス
        $message .= confirm::checkFuncs(array("checksInput", "checksMail", "checksMbLenMax", "checksAcount"), array("", "", array(40), array("")), $bnCharge->getData("mail"), "メールアドレス");

        // パスワード
        $message .= confirm::checkFuncs(array("checksInput", "checksMbLenWidth"), array("", array(4, 8)), $bnCharge->getData("password"), "パスワード");

        // 権限
        $message .= confirm::checkFuncs(array("checksInput", "checksMasterData"), array("", array("m_type_auth")), $bnCharge->getData("id_type_auth"), "権限");

        // ログイン制限
        $message .= confirm::checkFuncs(array("checksInput", "checksFlug"), array("", ""), $bnCharge->getData("flug_login_limit"), "ログイン制限");

        return $message;
    }

    // 担当者編集パラメータチェック
    function chkEdit($bnCharge) {
        $session = new session();
        $message = "";

        // 登録権限
        if (!confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) $message .= "編集出来る権限がありません。";

        // 店舗名
        $message .= confirm::checkFuncs(array("checksInput", "checksMasterData"), array("", array("t_store")), $bnCharge->getData("id_store"), "店舗名");

        // 担当者名
        $message .= confirm::checkFuncs(array("checksInput", "checksMbLenMax"), array("", array(20)), $bnCharge->getData("name"), "担当者名");

        // メールアドレス
        $message .= confirm::checkFuncs(array("checksInput", "checksMail", "checksMbLenMax", "checksAcount"), array("", "", array(40), array($bnCharge->getData("id"))), $bnCharge->getData("mail"), "メールアドレス");

        // パスワード
        $message .= (confirm::checkInput($bnCharge->getData("password"))) ? confirm::checkFuncs(array("checksInput", "checksMbLenWidth"), array("", array(4, 8)), $bnCharge->getData("password"), "パスワード") : "";

        // 権限
        $message .= confirm::checkFuncs(array("checksInput", "checksMasterData"), array("", array("m_type_auth")), $bnCharge->getData("id_type_auth"), "権限");

        // ログイン制限
        $message .= confirm::checkFuncs(array("checksInput", "checksFlug"), array("", ""), $bnCharge->getData("flug_login_limit"), "ログイン制限");

        return $message;
    }
}
?>

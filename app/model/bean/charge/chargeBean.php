<?php
require_once("web.php");
require_once(DIR_CLASS . "common/confirm.php");

class chargeBean {

    private $aryCharge = array();

    // 全データ設定
    function setDataAll($prmData) {
        while(list($key, $data) = each($prmData)) {
            $this->setData($key, $data);
        }
    }

    // 全データ取得
    function getDataAll() {
        $aryTemp = array();
        if(is_array($this->aryCharge)) {
            while(list($key, $data) = each($this->aryCharge)) {
                $aryTemp[$key] = $this->aryCharge[$key];
            }
        }

        return $aryTemp;
    }

    // データ設定
    function setData($prmName, $prmData) {
        $this->aryCharge[$prmName] = $prmData;
    }

    // データ取得
    function getData($prmName) {
        if (confirm::checkComp($prmName, "id_type_auth")) {
            if (!array_key_exists("id_type_auth", $this->aryCharge)) {
                $this->aryCharge["id_type_auth"] = "2";
            }
        }

        if (confirm::checkComp($prmName, "flug_login_limit")) {
            if (!array_key_exists("flug_login_limit", $this->aryCharge)) {
                $this->aryCharge["flug_login_limit"] = "0";
            }
        }

        if (confirm::checkComp($prmName, "login_limit_count")) {
            if (!array_key_exists("login_limit_count", $this->aryCharge)) {
                $this->aryCharge["login_limit_count"] = "0";
            }
        }

        if (!array_key_exists($prmName, $this->aryCharge)) {
            return NULL;
        }

        return $this->aryCharge[$prmName];
    }
}
?>

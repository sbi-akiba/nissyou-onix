<?php
require_once("web.php");
require_once(DIR_CLASS . "common/confirm.php");

class numberingBean {

    private $aryNumbering = array();

    // 全データ設定
    function setDataAll($prmData) {
        while(list($key, $data) = each($prmData)) {
            $this->setData($key, $data);
        }
    }

    // 全データ取得
    function getDataAll() {
        $aryTemp = array();
        if(is_array($this->aryNumbering)) {
            while(list($key, $data) = each($this->aryNumbering)) {
                $aryTemp[$key] = $this->aryNumbering[$key];
            }
        }

        return $aryTemp;
    }

    // データ設定
    function setData($prmName, $prmData) {
        $this->aryNumbering[$prmName] = $prmData;
    }

    // データ取得
    function getData($prmName) {
        if (!array_key_exists($prmName, $this->aryNumbering)) {
            return NULL;
        }

        return $this->aryNumbering[$prmName];
    }
}
?>

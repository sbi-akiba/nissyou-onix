<?php
require_once("web.php");
require_once(DIR_CLASS . "common/confirm.php");
require_once(DIR_CLASS . "common/convert.php");

class sessionCheckBean {

    function chkSession($session) {
        $message = "";
        $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");

        if (!confirm::checkInput($session->getData('id'))) {
            $message .= "ログアウトしました。再度ログインして下さい。\r\n";

        } else if ((!confirm::checkInput($session->getData('time'))) || (strtotime(date('Y-m-d H:i:s')) > strtotime(" +{$ini_array['hours']} hours {$ini_array['minutes']} minutes {$ini_array['seconds']} seconds", $session->getData('time')))) {
            $message .= "タイムアウトしました。再度ログインして下さい。\r\n";

        } else if (!confirm::checkComp($session->getData('host'), $_SERVER["REMOTE_ADDR"])) {
            $message .= "IPアドレスが変更された為、ログアウトしました。再度ログインして下さい。\r\n";
        }

        return $message;
    }
}
?>

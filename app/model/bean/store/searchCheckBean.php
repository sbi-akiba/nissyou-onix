<?php
require_once("web.php");
require_once(DIR_CLASS . "common/confirm.php");

class searchCheckBean {

// ------------------------------------------------

    // 店舗一覧検索パラメータチェック
    function chkSearch($bnSearch) {
        $message = "";

        // 開始ページ数値チェック
        if (!is_numeric($bnSearch->getData('page'))) {
            $message .= "ページ設定が不正です。<br>";

        // 開始ページ数値チェック
        } else if (confirm::checkComp($bnSearch->getData('page'), "0")) {
            $message .= "ページ設定が不正です。<br>";
        }

        return $message;
    }
}
?>

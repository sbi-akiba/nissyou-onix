<?php
require_once("web.php");
require_once(DIR_CLASS . "common/confirm.php");

class searchBean {

    private $arySearch = array();

    // 全データ設定
    function setDataAll($prmData) {
        while(list($key, $data) = each($prmData)) {
            $this->setData($key, $data);
        }
    }

    // 全データ取得
    function getDataAll() {
        $aryTemp = array();
        if(is_array($this->arySearch)) {
            while(list($key, $data) = each($this->arySearch)) {
                $aryTemp[$key] = $this->arySearch[$key];
            }
        }

        return $aryTemp;
    }

    // データ設定
    function setData($prmName, $prmData) {
        $this->arySearch[$prmName] = $prmData;
    }

    // データ取得
    function getData($prmName) {
        if (confirm::checkComp($prmName, "page")) {
            if (!array_key_exists("page", $this->arySearch)) {
                $this->arySearch["page"] = "1";
            }
        }

        if (!array_key_exists($prmName, $this->arySearch)) {
            return NULL;
        }

        return $this->arySearch[$prmName];
    }
}
?>

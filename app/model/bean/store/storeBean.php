<?php
require_once("web.php");
require_once(DIR_CLASS . "common/confirm.php");

class storeBean {

    private $aryStore = array();

    // 全データ設定
    function setDataAll($prmData) {
        while(list($key, $data) = each($prmData)) {
            $this->setData($key, $data);
        }
    }

    // 全データ取得
    function getDataAll() {
        $aryTemp = array();
        if(is_array($this->aryStore)) {
            while(list($key, $data) = each($this->aryStore)) {
                $aryTemp[$key] = $this->aryStore[$key];
            }
        }

        return $aryTemp;
    }

    // データ設定
    function setData($prmName, $prmData) {
        $this->aryStore[$prmName] = $prmData;
    }

    // データ取得
    function getData($prmName) {
        if (!array_key_exists($prmName, $this->aryStore)) {
            return NULL;
        }

        return $this->aryStore[$prmName];
    }
}
?>

<?php
require_once("web.php");
require_once(DIR_CLASS . "common/confirm.php");

class storeCheckBean {

// ------------------------------------------------

    // 店舗詳細パラメータチェック
    function chkDetail($bnStore) {
        $message = "";

        $message .= confirm::checkFuncs(array("checksInput", "checksSuuji"), array("", ""), $bnStore->getData("id"), "店舗ID");

        return $message;
    }

    // 店舗登録パラメータチェック
    function chkRegist($bnStore) {
        $session = new session();
        $message = "";

        // 登録権限
        if (!confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) $message .= "登録出来る権限がありません。";

        // 店舗番号
        $message .= confirm::checkFuncs(array("checksInput", "checksSuuji", "checksMbLenMax", "checksMasterData2"), array("", "", array("2"), array("t_store", "store_number")), $bnStore->getData("store_number"), "店舗番号");

        // 店舗名
        $message .= confirm::checkFuncs(array("checksInput", "checksMbLenMax"), array("", array("20")), $bnStore->getData("name"), "店舗名");

        // 郵便番号
        $message .= confirm::checkFuncs(array("checksInput", "checksZipCode"), array("", ""), $bnStore->getData("zip_code"), "郵便番号");

        // 住所1
        $message .= confirm::checkFuncs(array("checksInput", "checksMbLenMax"), array("", array("40")), $bnStore->getData("address_1"), "住所1");

        // 住所2
        $message .= confirm::checkInput($bnStore->getData("address_2")) ? confirm::checkFuncs(array("checksInput", "checksMbLenMax"), array("", array("40")), $bnStore->getData("address_2"), "住所2") : "";

        // 電話番号
        $message .= confirm::checkFuncs(array("checksInput", "checksTel"), array("", ""), $bnStore->getData("tel"), "電話番号");

        // FAX番号
        $message .= (confirm::checkInput($bnStore->getData("fax"))) ? confirm::checkFuncs(array("checksInput", "checksTel"), array("", ""), $bnStore->getData("fax"), "FAX番号") : "";

        return $message;
    }

    // 店舗編集パラメータチェック
    function chkEdit($bnStore) {
        $session = new session();
        $message = "";

        // 編集権限
        if (!confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) $message .= "編集出来る権限がありません。";

        // 店舗ID
        $message .= confirm::checkFuncs(array("checksInput", "checksSuuji"), array("", ""), $bnStore->getData("id"), "店舗ID");

        // 店舗名
        $message .= confirm::checkFuncs(array("checksInput", "checksMbLenMax"), array("", array("20")), $bnStore->getData("name"), "店舗名");

        // 郵便番号
        $message .= confirm::checkFuncs(array("checksInput", "checksZipCode"), array("", ""), $bnStore->getData("zip_code"), "郵便番号");

        // 住所1
        $message .= confirm::checkFuncs(array("checksInput", "checksMbLenMax"), array("", array("40")), $bnStore->getData("address_1"), "住所1");

        // 住所2
        $message .= confirm::checkInput($bnStore->getData("address_2")) ? confirm::checkFuncs(array("checksInput", "checksMbLenMax"), array("", array("40")), $bnStore->getData("address_2"), "住所2") : "";

        // 電話番号
        $message .= confirm::checkFuncs(array("checksInput", "checksTel"), array("", ""), $bnStore->getData("tel"), "電話番号");

        // FAX番号
        $message .= (confirm::checkInput($bnStore->getData("fax"))) ? confirm::checkFuncs(array("checksInput", "checksTel"), array("", ""), $bnStore->getData("fax"), "FAX番号") : "";

        return $message;
    }
}
?>

<?php
require_once("web.php");
require_once(DIR_CLASS . "common/confirm.php");

class receiptBean {

    private $aryReceipt = array();

    // 全データ設定
    function setDataAll($prmData) {
        while(list($key, $data) = each($prmData)) {
            $this->setData($key, $data);
        }
    }

    // 全データ取得
    function getDataAll() {
        $aryTemp = array();
        if(is_array($this->aryReceipt)) {
            while(list($key, $data) = each($this->aryReceipt)) {
                $aryTemp[$key] = $this->aryReceipt[$key];
            }
        }

        return $aryTemp;
    }

    // データ設定
    function setData($prmName, $prmData) {
        $this->aryReceipt[$prmName] = $prmData;
    }

    // データ取得
    function getData($prmName) {
        if (confirm::checkComp($prmName, "page")) {
            if (!array_key_exists("page", $this->aryReceipt)) {
                $this->aryReceipt["page"] = "1";
            }
        }

        if (!array_key_exists($prmName, $this->aryReceipt)) {
            return NULL;
        }

        return $this->aryReceipt[$prmName];
    }
}
?>

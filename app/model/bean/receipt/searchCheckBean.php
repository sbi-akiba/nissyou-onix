<?php
require_once("web.php");
require_once(DIR_CLASS . "common/confirm.php");

class searchCheckBean {

// ------------------------------------------------

    // 領収書一覧検索パラメータチェック
    function chkSearch($bnSearch) {
        $message = "";

        // 発行日チェック
        $message .= confirm::checkInput($bnSearch->getData("date_issue_01")) ? confirm::checkFuncs(array("checksDate"), array(""), $bnSearch->getData("date_issue_01"), "発行日（開始）") : "";
        $message .= confirm::checkInput($bnSearch->getData("date_issue_02")) ? confirm::checkFuncs(array("checksDate"), array(""), $bnSearch->getData("date_issue_02"), "発行日（終了）") : "";

        // 印刷設定
        $message .= confirm::checkInput($bnSearch->getData("flug_print")) ? confirm::checkFuncs(array("checksFlug"), array(""), $bnSearch->getData("flug_print"), "印刷") : "";

        // 店舗名設定
        $message .= confirm::checkInput($bnSearch->getData("id_store")) ? confirm::checkFuncs(array("checksMasterData"), array(array("t_store")), $bnSearch->getData("id_store"), "店舗名") : "";

        // 担当者名設定
        $message .= confirm::checkInput($bnSearch->getData("id_charge")) ? confirm::checkFuncs(array("checksMasterData"), array(array("t_charge")), $bnSearch->getData("id_charge"), "担当者名") : "";

        // お客さま名設定

        // 但し書き設定

        // 金額設定

        // 備考設定

        // 開始ページ数値チェック
        if (!is_numeric($bnSearch->getData('page'))) {
            $message .= "ページ設定が不正です。<br>";

        // 開始ページ数値チェック
        } else if (confirm::checkComp($bnSearch->getData('page'), "0")) {
            $message .= "ページ設定が不正です。<br>";
        }

        return $message;
    }

    // 領収書一覧CSVパラメータチェック
    function chkCsv($bnSearch) {
        $message = "";

        // 表示権限
        if (!confirm::checkComp($session->getData('id_type_auth'), LEVEL_ALL_STORE)) $message .= "出力出来る権限がありません。";

        $message .= $this->chkSearch($bnSearch);

        return $message;
    }

    // 領収書一覧PDFパラメータチェック
    function chkPdf($bnSearch) {
        $session = new session();
        $message = "";

        $message .= $this->chkSearch($bnSearch);

        return $message;
    }
}
?>

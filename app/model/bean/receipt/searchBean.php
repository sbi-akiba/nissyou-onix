<?php
require_once("web.php");
require_once(DIR_CLASS . "common/confirm.php");

class searchBean {

    private $arySsearch = array();

    // 全データ設定
    function setDataAll($prmData) {
        while(list($key, $data) = each($prmData)) {
            $this->setData($key, $data);
        }
    }

    // 全データ取得
    function getDataAll() {
        $aryTemp = array();
        if(is_array($this->arySsearch)) {
            while(list($key, $data) = each($this->arySsearch)) {
                $aryTemp[$key] = $this->arySsearch[$key];
            }
        }

        return $aryTemp;
    }

    // データ設定
    function setData($prmName, $prmData) {
        $this->arySsearch[$prmName] = $prmData;
    }

    // データ取得
    function getData($prmName) {
        if (confirm::checkComp($prmName, "flug_print")) {
            if (!array_key_exists("flug_print", $this->arySsearch)) {
                $this->arySsearch["flug_print"] = "";
            }
        }

        if (confirm::checkComp($prmName, "id_store")) {
            if (!array_key_exists("id_store", $this->arySsearch)) {
                $this->arySsearch["id_store"] = "";
            }
        }

        if (confirm::checkComp($prmName, "id_charge")) {
            if (!array_key_exists("id_charge", $this->arySsearch)) {
                $this->arySsearch["id_charge"] = "";
            }
        }

        if (confirm::checkComp($prmName, "sub")) {
            if (!array_key_exists("sub", $this->arySsearch)) {
                $this->arySsearch["sub"] = "0";
            }
        }

        if (confirm::checkComp($prmName, "page")) {
            if (!array_key_exists("page", $this->arySsearch)) {
                $this->arySsearch["page"] = "1";
            }
        }

        if (!array_key_exists($prmName, $this->arySsearch)) {
            return NULL;
        }

        return $this->arySsearch[$prmName];
    }
}
?>

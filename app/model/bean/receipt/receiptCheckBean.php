<?php
require_once("web.php");
require_once(DIR_CLASS . "common/confirm.php");

class receiptCheckBean {

// ------------------------------------------------

    // 領収書詳細パラメータチェック
    function chkDetail($bnReceipt) {
        $message = "";

        // 領収書ID
        $message .= confirm::checkFuncs(array("checksInput", "checksSuuji"), array("", ""), $bnReceipt->getData("id"), "領収書ID");

        return $message;
    }

    // 領収書登録パラメータチェック
    function chkRegist($bnReceipt) {
        $message = "";

        // お客さま名
        $message .= confirm::checkFuncs(array("checksInput", "checksMbLenMax"), array("", array("20")), $bnReceipt->getData("customer"), "お客さま名");

        // 敬称
        $message .= confirm::checkFuncs(array("checksInput", "checksMasterData"), array("", array("m_type_honorific")), $bnReceipt->getData("id_type_honorific"), "敬称");

        // 金額
        $message .= confirm::checkFuncs(array("checksInput", "checksSuuji", "checksMbLenMax"), array("", "", array(10)), $bnReceipt->getData("amount"), "金額");

        // 消費税
        $message .= confirm::checkFuncs(array("checksInput", "checksSuuji", "checksMbLenMax"), array("", "", array(10)), $bnReceipt->getData("tax"), "消費税");

        // 品書き
        $message .= confirm::checkFuncs(array("checksInput", "checksMbLenMax"), array("", array("80")), $bnReceipt->getData("proviso"), "品書き");

        // 備考
        $message .= (confirm::checkInput($bnReceipt->getData("note"))) ? confirm::checkFuncs(array("checksMbLenMax"), array(array("200")), $bnReceipt->getData("note"), "備考") : "";

        return $message;
    }

    // 領収書編集パラメータチェック
    function chkEdit($bnReceipt) {
        $message = "";

        // お客さま名
        $message .= confirm::checkFuncs(array("checksInput", "checksMbLenMax"), array("", array("20")), $bnReceipt->getData("customer"), "お客さま名");

        // 敬称
        $message .= confirm::checkFuncs(array("checksInput", "checksMasterData"), array("", array("m_type_honorific")), $bnReceipt->getData("id_type_honorific"), "敬称");

        // 金額
        $message .= confirm::checkFuncs(array("checksInput", "checksSuuji", "checksMbLenMax"), array("", "", array(10)), $bnReceipt->getData("amount"), "金額");

        // 消費税
        $message .= confirm::checkFuncs(array("checksInput", "checksSuuji", "checksMbLenMax"), array("", "", array(10)), $bnReceipt->getData("tax"), "消費税");

        // 品書き
        $message .= confirm::checkFuncs(array("checksInput", "checksMbLenMax"), array("", array("80")), $bnReceipt->getData("proviso"), "品書き");

        // 備考
        $message .= (confirm::checkInput($bnReceipt->getData("note"))) ? confirm::checkFuncs(array("checksMbLenMax"), array(array("200")), $bnReceipt->getData("note"), "備考") : "";

        return $message;
    }

    // 領収書削除パラメータチェック
    function chkDelete($bnReceipt) {
        $message = "";

        // 領収書ID
        $message .= confirm::checkFuncs(array("checksInput", "checksSuuji"), array("", ""), $bnReceipt->getData("id"), "領収書ID");

        // 削除状態
        $message .= confirm::checkFuncs(array("checksDelete"), array(""), $bnReceipt->getData("id"), "削除");

        return $message;
    }

    // 領収書印刷パラメータチェック
    function chkPrint($bnReceipt) {
        $message = "";

        // 領収書ID
        $message .= confirm::checkFuncs(array("checksInput", "checksSuuji"), array("", ""), $bnReceipt->getData("id"), "領収書ID");

        // 印刷状態
        $message .= confirm::checkFuncs(array("checksPrint"), array(""), $bnReceipt->getData("id"), "印刷");

        return $message;
    }
}
?>

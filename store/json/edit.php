<?php
    require_once("web.php");
    require_once(DIR_CLASS . "common/html.php");
    require_once(DIR_CLASS . "common/confirm.php");
    require_once(DIR_CLASS . "common/convert.php");
    require_once(DIR_CLASS . "bean/store/storeBean.php");
    require_once(DIR_CLASS . "bean/store/storeCheckBean.php");
    require_once(DIR_CLASS . "dao/store/storeDAO.php");
    require_once(DIR_CLASS . "common/responseJson.php");

    // セッションチェック
    require_once(DIR_CLASS . "filter/ajaxFilter.php");
    require_once(DIR_CLASS . "filter/sessionFilter.php");

    // 初期処理
    $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
    $session = new session();
    $bnStore = new storeBean();
    $resJson = new responseJson();

    // パラメータ取得
    $bnStore->setDataAll($_POST);

    // パラメータチェック
    $bnStoreCheck = new storeCheckBean();
    $message = $bnStoreCheck->chkEdit($bnStore);

    // エラーの場合
    if (confirm::checkInput($message)) {
        $resJson->setArray(array('status'=>'1', 'message'=>convert::unescapeLine($message)));
        $resJson->send();
        exit;
    }

    // 店舗情報設定
    $daoStore = new storeDAO();
    $intCount = $daoStore->chgOne($bnStore);

    // エラーの場合
    if ($intCount != 1) {
        $resJson->setArray(array('status'=>'1', 'message'=>'店舗データの設定に失敗しました。'));
        $resJson->send();
        exit;
    }

    $bnStore->setData("status", "0");
    $resJson->setArray($bnStore->getDataAll());
    $resJson->send();
?>

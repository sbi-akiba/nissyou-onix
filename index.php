<?php
    require_once("web.php");
    require_once(DIR_CLASS . "common/confirm.php");
    require_once(DIR_CLASS . "common/convert.php");
    require_once(DIR_CLASS . "common/session.php");
    require_once(DIR_CLASS . "bean/charge/chargeBean.php");

    // 初期処理
    $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");

    // パラメータ取得
    $filter_session = new session();
    $bnCharge = new chargeBean();
    $message = "";

    // セッションエラーチェック
    if (confirm::checkInput($filter_session->getData('message'))) {
        $message = $filter_session->getData('message');
        $filter_session->close();
    }

    // ページ表示
    require_once(DIR_VIEW . "include/chache.php");
    require_once(DIR_VIEW . "index.php");
?>

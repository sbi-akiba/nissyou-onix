<?php
    require_once("web.php");
    require_once(DIR_CLASS . "common/html.php");
    require_once(DIR_CLASS . "common/confirm.php");
    require_once(DIR_CLASS . "common/convert.php");
    require_once(DIR_CLASS . "dao/store/storeDAO.php");
    require_once(DIR_CLASS . "dao/charge/chargeDAO.php");
    require_once(DIR_CLASS . "bean/receipt/searchBean.php");
    require_once(DIR_CLASS . "bean/receipt/searchCheckBean.php");
    require_once(DIR_CLASS . "dao/receipt/searchDAO.php");
    require_once(DIR_CLASS . "dao/type/typeHonorificDAO.php");

    // セッションチェック
    require_once(DIR_CLASS . "filter/sessionFilter.php");

    // 初期処理
    $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
    $session = new session();
    $bnSearch = new searchBean();
    $arySearch = array();

    // パラメータ取得
    $bnSearch->setDataAll($_POST);

    // パラメータチェック
    $bnSearchCheck = new searchCheckBean();
    $message = $bnSearchCheck->chkSearch($bnSearch);

    // エラーの場合
    if (confirm::checkInput($message)) {
        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "error/popup.php");
        exit;
    }

    // 領収書情報取得
    $daoSearch = new searchDAO();
    $bnSearch->setData("page", "");
    $intCount = $daoSearch->getAllCount($bnSearch);

    if ($intCount > $ini_array["csv_list_max"]) {
        $message = "領収書数が{$ini_array["csv_list_max"]}件を超えているため、CSV化出来ません。";

        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "error/popup.php");
        exit;
    }

    $arySearch = $daoSearch->getAllCsv($bnSearch);

    // CSV出力
    $header = "番号,日付,店舗名,担当者名,お客さま名,税込金額,品書き,備考,印刷\r\n";

    header("Content-Type: application/octet-stream");
    header("Content-disposition: attachment; filename=".date("Ymd_His").".csv");

    // ヘッダー出力
    echo mb_convert_encoding($header, 'SJIS-win', mb_internal_encoding());

    // 本体出力
    $stream = fopen('php://output', 'w');
    mb_convert_variables('SJIS-win', mb_internal_encoding(), $arySearch);

    foreach($arySearch as $csv){
        fputcsv($stream, $csv);
    }

    fclose($stream);
    exit;
?>

<?php
    require_once("web.php");
    require_once(DIR_CLASS . "common/html.php");
    require_once(DIR_CLASS . "common/confirm.php");
    require_once(DIR_CLASS . "common/convert.php");
    require_once(DIR_CLASS . "bean/receipt/receiptBean.php");
    require_once(DIR_CLASS . "bean/receipt/receiptCheckBean.php");
    require_once(DIR_CLASS . "dao/receipt/receiptDAO.php");
    require_once(DIR_CLASS . "common/responseJson.php");

    // セッションチェック
    require_once(DIR_CLASS . "filter/ajaxFilter.php");
    require_once(DIR_CLASS . "filter/sessionFilter.php");

    // 初期処理
    $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
    $session = new session();
    $bnReceipt = new receiptBean();
    $bnReceiptTemp = new receiptBean();
    $resJson = new responseJson();

    // パラメータ取得
    $bnReceipt->setDataAll($_POST);

    // パラメータチェック
    $bnReceiptCheck = new receiptCheckBean();
    $message = $bnReceiptCheck->chkEdit($bnReceipt);

    // エラーの場合
    if (confirm::checkInput($message)) {
        $resJson->setArray(array('status'=>'1', 'message'=>convert::unescapeLine($message)));
        $resJson->send();
        exit;
    }

    // 領収書情報取得
    $daoReceipt = new receiptDAO();
    $bnReceiptTemp = $daoReceipt->getOne($bnReceipt);

    // エラーの場合
    if (!confirm::checkInput($bnReceiptTemp->getData('id'))) {
        $resJson->setArray(array('status'=>'1', 'message'=>'領収書データの取得に失敗しました。'));
        $resJson->send();
        exit;
    }

    // 管理者にて印刷済領収書の編集の場合
    if ((confirm::checkComp($session->getData("id_type_auth"), LEVEL_ALL_STORE)) && (confirm::checkComp($bnReceiptTemp->getData("flug_print"), "1"))) {
        // 領収書情報設定（店舗・管理者未更新）
        $intCount = $daoReceipt->chgOne2($bnReceipt);
    } else {
        // 領収書情報設定
        $intCount = $daoReceipt->chgOne($bnReceipt);
    }

    // エラーの場合
    if ($intCount != 1) {
        $resJson->setArray(array('status'=>'1', 'message'=>'領収書データの設定に失敗しました。'));
        $resJson->send();
        exit;
    }

    $bnReceipt->setData("status", "0");
    $resJson->setArray($bnReceipt->getDataAll());
    $resJson->send();
?>

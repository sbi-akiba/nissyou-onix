<?php
    require_once("web.php");
    require_once(DIR_CLASS . "common/html.php");
    require_once(DIR_CLASS . "common/confirm.php");
    require_once(DIR_CLASS . "common/convert.php");
    require_once(DIR_CLASS . "bean/receipt/receiptBean.php");
    require_once(DIR_CLASS . "bean/receipt/receiptCheckBean.php");
    require_once(DIR_CLASS . "dao/receipt/receiptDAO.php");
    require_once(DIR_CLASS . "common/responseJson.php");

    // セッションチェック
    require_once(DIR_CLASS . "filter/ajaxFilter.php");
    require_once(DIR_CLASS . "filter/sessionFilter.php");

    // 初期処理
    $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
    $session = new session();
    $bnReceipt = new receiptBean();
    $resJson = new responseJson();

    // パラメータ取得
    $bnReceipt->setDataAll($_POST);

    // パラメータチェック
    $bnReceiptCheck = new receiptCheckBean();
    $message = $bnReceiptCheck->chkDetail($bnReceipt);

    // エラーの場合
    if (confirm::checkInput($message)) {
        $resJson->setArray(array('status'=>'1', 'message'=>convert::unescapeLine($message)));
        $resJson->send();
        exit;
    }

    // 領収書情報取得
    $daoReceipt = new receiptDAO();
    $bnReceipt = $daoReceipt->getOne($bnReceipt);

    // エラーの場合
    if (!confirm::checkInput($bnReceipt->getData('id'))) {
        $resJson->setArray(array('status'=>'1', 'message'=>'領収書データの取得に失敗しました。'));
        $resJson->send();
        exit;
    }

    $bnReceipt->setData("status", 0);
    $resJson->setArray($bnReceipt->getDataAll());
    $resJson->send();
?>

<?php
    require_once("web.php");
    require_once(DIR_LIB . "mpdf/mpdf.php");
    require_once(DIR_CLASS . "common/html.php");
    require_once(DIR_CLASS . "common/confirm.php");
    require_once(DIR_CLASS . "common/convert.php");
    require_once(DIR_CLASS . "bean/store/storeBean.php");
    require_once(DIR_CLASS . "dao/store/storeDAO.php");
    require_once(DIR_CLASS . "bean/charge/chargeBean.php");
    require_once(DIR_CLASS . "dao/charge/chargeDAO.php");
    require_once(DIR_CLASS . "bean/receipt/searchBean.php");
    require_once(DIR_CLASS . "bean/receipt/searchCheckBean.php");
    require_once(DIR_CLASS . "dao/receipt/searchDAO.php");
    require_once(DIR_CLASS . "dao/type/typeHonorificDAO.php");

    // セッションチェック
    require_once(DIR_CLASS . "filter/sessionFilter.php");

    // 初期処理
    ini_set('memory_limit', '128M');
    $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
    $session = new session();
    $bnSearch = new searchBean();
    $arySearch = array();
    $mpdf = new mpdf('ja+aCJK','A4-L',0,'',10,10,8,8,0,0);
    $mpdf->setTitle("領収書一覧");

    // パラメータ取得
    $bnSearch->setDataAll($_GET);

    // パラメータチェック
    $bnSearchCheck = new searchCheckBean();
    $message = $bnSearchCheck->chkPdf($bnSearch);

    // エラーの場合
    if (confirm::checkInput($message)) {
        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "error/popup.php");
        exit;
    }

    // 領収書情報取得
    $daoSearch = new searchDAO();
    $bnSearch->setData("page", "");
    $intCount = $daoSearch->getAllCount($bnSearch);

    if ($intCount > $ini_array["pdf_list_max"]) {
        $message = "領収書数が{$ini_array["pdf_list_max"]}件を超えているため、PDF化出来ません。";

        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "error/popup.php");
        exit;
    }

    $arySearch = $daoSearch->getAll($bnSearch);
    $intAmount = $daoSearch->getAllAmount($bnSearch);

    // 店舗情報取得
    $daoStore = new storeDAO();
    $aryStore = $daoStore->getAllSimple();
    $aryStore = convert::escapeXSS($aryStore);
    array_unshift($aryStore, array('id' => '', 'value' => ''));

    // 担当者情報取得
    $daoCharge = new chargeDAO();
    $aryCharge = $daoCharge->getAllSimple();
    $aryCharge = convert::escapeXSS($aryCharge);
    array_unshift($aryCharge, array('id' => '', 'value' => ''));

    while (count($arySearch) > 0) {
        $aryData = array();
        $countPage++;
        for ($cntReceipt = 0; $cntReceipt < $ini_array["pdf_list_max_page"]; $cntReceipt++) {
            $aryData[] = array_shift($arySearch);
            if (count($arySearch) == 0) break;
        }

        ob_start();
        require(DIR_VIEW . "receipt/pdf/list.php");
        $mpdf->WriteHTML(ob_get_clean());

        if (count($arySearch) != 0) $mpdf->AddPage();
    }

    // PDF出力
    $mpdf->Output();
    exit;
?>

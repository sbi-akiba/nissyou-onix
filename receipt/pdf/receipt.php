<?php
    require_once("web.php");
    require_once(DIR_LIB . "mpdf/mpdf.php");
    require_once(DIR_CLASS . "common/confirm.php");
    require_once(DIR_CLASS . "common/convert.php");
    require_once(DIR_CLASS . "bean/receipt/receiptBean.php");
    require_once(DIR_CLASS . "bean/receipt/receiptCheckBean.php");
    require_once(DIR_CLASS . "dao/receipt/receiptDAO.php");
    require_once(DIR_CLASS . "bean/charge/chargeBean.php");
    require_once(DIR_CLASS . "dao/charge/chargeDAO.php");
    require_once(DIR_CLASS . "bean/store/storeBean.php");
    require_once(DIR_CLASS . "dao/store/storeDAO.php");
    require_once(DIR_CLASS . "bean/numbering/numberingBean.php");
    require_once(DIR_CLASS . "dao/numbering/numberingDAO.php");

    // セッションチェック
    require_once(DIR_CLASS . "filter/sessionFilter.php");

    // 初期処理
    ini_set('memory_limit', '128M');
    $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
    $session = new session();
    $bnReceipt = new receiptBean();
    $mpdf = new mPDF('ja+aCJK','A4',0,'',10,10,8,8,0,0);
    $mpdf->setTitle("領収書");

    // パラメータ取得
    $bnReceipt->setDataAll($_GET);

    // パラメータチェック
    $bnReceiptCheck = new receiptCheckBean();
    $message = $bnReceiptCheck->chkPrint($bnReceipt);

    // エラーの場合
    if (confirm::checkInput($message)) {
        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "error/popup.php");
        exit;
    }

    // 採番情報取得
    $daoNumbering = new numberingDAO();
    $bnNumbering = new numberingBean();
    $bnNumbering->setData('id_store', $session->getData('id_store'));
    $intNumber = $daoNumbering->setOne($bnNumbering);

    // 採番生成（YY-MM-店舗番号-採番番号）
    $strReceiptNumber = date("y") . "-" . 
                        date("m") . "-" . 
                        str_pad($session->getData('store_number'), 2, 0, STR_PAD_LEFT) . "-" . 
                        str_pad($intNumber, 4, 0, STR_PAD_LEFT);

    $bnReceipt->setData('receipt_number', $strReceiptNumber);

    // 採番・印刷日・印刷フラグ・担当者設定
    $daoReceipt = new receiptDAO();
    $daoReceipt->setPrint($bnReceipt);

    // 領収書情報取得
    $bnReceipt = $daoReceipt->getOne($bnReceipt);

    // エラーの場合
    if (!confirm::checkInput($bnReceipt->getData('id'))) {
        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "error/popup.php");
        exit;
    }

    // 担当者情報取得
    $bnCharge = new chargeBean();
    $daoCharge = new chargeDAO();
    $bnCharge->setData("id", $bnReceipt->getData("id_charge"));
    $bnCharge = $daoCharge->getOne($bnCharge);

    // エラーの場合
    if (!confirm::checkInput($bnCharge->getData('id'))) {
        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "error/popup.php");
        exit;
    }

    // 店舗情報取得
    $bnStore = new storeBean();
    $daoStore = new storeDAO();
    $bnStore->setData("id", $bnReceipt->getData("id_store"));
    $bnStore = $daoStore->getOne($bnStore);

    // エラーの場合
    if (!confirm::checkInput($bnStore->getData('id'))) {
        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "error/popup.php");
        exit;
    }

    ob_start();
    $stub = 0;
    require(DIR_VIEW . "receipt/pdf/receipt.php");
    $mpdf->WriteHTML(ob_get_clean());

    ob_start();
    $mpdf->AddPage();
    $stub = 1;
    require(DIR_VIEW . "receipt/pdf/receipt_stub.php");
    $mpdf->WriteHTML(ob_get_clean());

    // PDF出力
    $mpdf->Output();
    exit;
?>

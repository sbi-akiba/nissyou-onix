<?php
    require_once("web.php");
    require_once(DIR_CLASS . "common/confirm.php");
    require_once(DIR_CLASS . "common/convert.php");
    require_once(DIR_CLASS . "bean/receipt/receiptBean.php");
    require_once(DIR_CLASS . "bean/receipt/receiptCheckBean.php");
    require_once(DIR_CLASS . "bean/receipt/searchBean.php");
    require_once(DIR_CLASS . "bean/receipt/searchCheckBean.php");
    require_once(DIR_CLASS . "dao/receipt/searchDAO.php");

    // セッションチェック
    require_once(DIR_CLASS . "filter/sessionFilter.php");

    // 初期処理
    $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
    $session = new session();
    $strUrl = "";

    // パラメータチェック
    if (empty($_POST['output'])) {
        $message = "ページを更新後、再度お試し下さい。";

    } else if (confirm::checkComp(PDF_RECEIPT, $_POST['output'])) {
        $bnReceipt = new receiptBean();
        $bnReceipt->setDataAll($_POST);
        $bnReceiptCheck = new receiptCheckBean();
        $message = convert::unescapeLine($bnReceiptCheck->chkPrint($bnReceipt));

    } else if (confirm::checkComp(PDF_LIST, $_POST['output'])) {
        $bnSearch = new searchBean();
        $bnSearch->setDataAll($_POST);
        $bnSearchCheck = new searchCheckBean();
        $message = convert::unescapeLine($bnSearchCheck->chkPdf($bnSearch));

        if (!confirm::checkInput($message)) {
            $daoSearch = new searchDAO();
            $bnSearch->setData("page", "");
            $intCount = $daoSearch->getAllCount($bnSearch);

            if ($intCount > $ini_array["pdf_list_max"]) {
                $message = "領収書数が{$ini_array["pdf_list_max"]}件を超えているため、PDF化出来ません。";
            }
        }

    } else {
        $message = "PDFを出力出来ません。";
    }

    // エラーの場合
    if (confirm::checkInput($message)) {
        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "error/popup.php");
        exit;
    }

    // パラメータ取得
    if (confirm::checkComp(PDF_LIST, $_POST['output'])) {
        $strUrl = "/receipt/pdf/list.php?" . http_build_query($_POST);
    } else if (confirm::checkComp(PDF_RECEIPT, $_POST['output'])) {
        $strUrl = "/receipt/pdf/receipt.php?" . http_build_query($_POST);
    }

    // ページ表示
    require_once(DIR_VIEW . "receipt/pdf/index.php");
?>

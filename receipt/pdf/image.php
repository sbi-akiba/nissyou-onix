<?php
    require_once("web.php");
    require_once(DIR_CLASS . "common/confirm.php");
    require_once(DIR_CLASS . "common/convert.php");
    require_once(DIR_CLASS . "bean/receipt/receiptBean.php");
    require_once(DIR_CLASS . "bean/receipt/receiptCheckBean.php");
    require_once(DIR_CLASS . "dao/receipt/receiptDAO.php");
    require_once(DIR_CLASS . "bean/charge/chargeBean.php");
    require_once(DIR_CLASS . "dao/charge/chargeDAO.php");
    require_once(DIR_CLASS . "bean/store/storeBean.php");
    require_once(DIR_CLASS . "dao/store/storeDAO.php");

    // セッションチェック
    require_once(DIR_CLASS . "filter/sessionFilter.php");

    // 初期処理
    $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
    $session = new session();
    $bnReceipt = new receiptBean();

    // パラメータ取得
    $bnReceipt->setDataAll($_GET);

    // パラメータチェック
    $bnReceiptCheck = new receiptCheckBean();
    $message = $bnReceiptCheck->chkDetail($bnReceipt);

    // エラーの場合
    if (confirm::checkInput($message)) {
        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "error/popup.php");
        exit;
    }

    // 領収書情報取得
    $daoReceipt = new receiptDAO();
    $bnReceipt = $daoReceipt->getOne($bnReceipt);
    $bnReceipt->setDataAll(convert::escapeXSS($bnReceipt->getDataAll()));

    // エラーの場合
    if (!confirm::checkInput($bnReceipt->getData('id'))) {
        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "error/popup.php");
        exit;
    }

    // 担当者情報取得
    $bnCharge = new chargeBean();
    $daoCharge = new chargeDAO();
    $bnCharge->setData("id", $session->getData("id"));
    $bnCharge = $daoCharge->getOne($bnCharge);
    $bnCharge->setDataAll(convert::escapeXSS($bnCharge->getDataAll()));

    // エラーの場合
    if (!confirm::checkInput($bnCharge->getData('id'))) {
        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "error/popup.php");
        exit;
    }

    // 店舗情報取得
    $bnStore = new storeBean();
    $daoStore = new storeDAO();
    $bnStore->setData("id", $session->getData("id_store"));
    $bnStore = $daoStore->getOne($bnStore);
    $bnStore->setDataAll(convert::escapeXSS($bnStore->getDataAll()));

    // エラーの場合
    if (!confirm::checkInput($bnStore->getData('id'))) {
        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "error/popup.php");
        exit;
    }

    // ページ表示
    require_once(DIR_VIEW . "include/chache.php");
    require_once(DIR_VIEW . "receipt/pdf/image.php");
?>

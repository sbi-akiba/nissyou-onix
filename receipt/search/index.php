<?php
    require_once("web.php");
    require_once(DIR_CLASS . "common/html.php");
    require_once(DIR_CLASS . "common/confirm.php");
    require_once(DIR_CLASS . "common/convert.php");
    require_once(DIR_CLASS . "dao/store/storeDAO.php");
    require_once(DIR_CLASS . "dao/charge/chargeDAO.php");
    require_once(DIR_CLASS . "bean/receipt/searchBean.php");
    require_once(DIR_CLASS . "bean/receipt/searchCheckBean.php");
    require_once(DIR_CLASS . "dao/receipt/searchDAO.php");
    require_once(DIR_CLASS . "dao/type/typeHonorificDAO.php");

    // セッションチェック
    require_once(DIR_CLASS . "filter/sessionFilter.php");

    // 初期処理
    $ini_array = parse_ini_file(DIR_CONFIG . "system.ini");
    $session = new session();
    $bnSearch = new searchBean();
    $arySearch = array();
    $intCount = 0;
    $intAmount = 0;

    // パラメータ取得
    $bnSearch->setDataAll($_GET);

    // 店舗情報取得
    $daoStore = new storeDAO();
    $aryStore = $daoStore->getAllSimple();
    $aryStore = convert::escapeXSS($aryStore);
    array_unshift($aryStore, array('id' => '', 'value' => ''));

    // 担当者情報取得
    $daoCharge = new chargeDAO();
    $aryCharge = $daoCharge->getAllSimple();
    $aryCharge = convert::escapeXSS($aryCharge);
    array_unshift($aryCharge, array('id' => '', 'value' => ''));

    // 敬称区分取得
    $daoTypeHonorific = new typeHonorificDAO();
    $aryTypeHonorific = $daoTypeHonorific->getAllSimple();
    $aryTypeHonorific = convert::escapeXSS($aryTypeHonorific);

    // パラメータチェック
    $bnSearchCheck = new searchCheckBean();
    $message = $bnSearchCheck->chkSearch($bnSearch);

    // エラーの場合
    if (confirm::checkInput($message)) {
        require_once(DIR_VIEW . "include/chache.php");
        require_once(DIR_VIEW . "receipt/search/index.php");
        exit;
    }

    // 領収書情報取得
    $daoSearch = new searchDAO();
    $arySearch = $daoSearch->getAll($bnSearch);
    $arySearch = convert::escapeXSS($arySearch);
    $intCount = $daoSearch->getAllCount($bnSearch);
    $intAmount = $daoSearch->getAllAmount($bnSearch);

    // ページ表示
    require_once(DIR_VIEW . "include/chache.php");
    require_once(DIR_VIEW . "receipt/search/index.php");
?>
